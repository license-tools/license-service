# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#
cmake_minimum_required(VERSION 3.5)


function(strip_binary TARGET)
    if (NOT STRIP_BINARIES)
        return()
    endif()

    message(STATUS "Stripping binary: ${TARGET} for: ${CONFIG}")
    add_custom_command(
        TARGET "${TARGET}" POST_BUILD
        DEPENDS "${TARGET}"
        COMMAND $<$<CONFIG:release>:${CMAKE_STRIP}>
        ARGS --strip-all $<TARGET_FILE:${TARGET}>
    )
endfunction()

function(version_compare result version1 op version2)
    if (version1 VERSION_EQUAL version2)
        if ("${op}" STREQUAL "EQUAL" OR "${op}" STREQUAL "LE" OR "${op}" STREQUAL "GE")
            set(${result} "1" PARENT_SCOPE)
        else ()
            set(${result} "0" PARENT_SCOPE)
        endif ()
    elseif (version1 VERSION_LESS version2)
        if ("${op}" STREQUAL "LESS" OR "${op}" STREQUAL "LE")
            set(${result} "1" PARENT_SCOPE)
        else ()
            set(${result} "0" PARENT_SCOPE)
        endif ()
    elseif (version1 VERSION_GREATER version2)
        if ("${op}" STREQUAL "GREATER" OR "${op}" STREQUAL "GE")
            set(${result} "1" PARENT_SCOPE)
        else ()
            set(${result} "0" PARENT_SCOPE)
        endif ()
    endif ()
endfunction()

function(get_xcode_version xcode_version)
    execute_process(
        COMMAND xcodebuild -version
        OUTPUT_VARIABLE XCODE_VERSION_OUTPUT
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    string(REGEX MATCH "Xcode ([0-9]+\\.[0-9]+)" _ ${XCODE_VERSION_OUTPUT})
    set(${xcode_version} ${CMAKE_MATCH_1} PARENT_SCOPE)
endfunction()
