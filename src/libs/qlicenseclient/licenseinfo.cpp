/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseinfo.h"

namespace QLicenseClient {

/*
    \class LicenseInfo

    \brief The LicenseInfo class provides methods for retrieving information about
           a license.
*/

/*
    Returns the licensee information of this license.
*/
const LicenseeInfo &LicenseInfo::licenseeInfo() const
{
    return m_licensee;
}

/*
    Returns the contacts information of this license.
*/
const LicenseeContacts &LicenseInfo::contactsInfo() const
{
    return m_contacts;
}

/*
    Finds license information of \a type, which can be retrieved with the
    return parameter \a value.

    Returns \c true if information for \a type was found, \c false otherwise.
*/
bool LicenseInfo::licenseInfo(const LicenseInfoType type, std::string &value) const
{
    if (m_licenseInfo.find(type) != m_licenseInfo.end()) {
        value = m_licenseInfo.at(type);
        return true;
    }
    return false;
}

/*
    Returns a list of possible consumers of this license.
*/
std::vector<ConsumerInfo> LicenseInfo::consumers() const
{
    return m_consumers;
}

/*
    Returns the raw license JSON data received from license service.
*/
const std::string &LicenseInfo::jsonData() const
{
    return m_jsonData;
}

/*
    Sets the licensee information of this license to \a licensee.
*/
void LicenseInfo::setLicensee(const LicenseeInfo &licensee)
{
    m_licensee = licensee;
}

/*
    Sets the contacts information of this license to \a contacts.
*/
void LicenseInfo::setContacts(const LicenseeContacts &contacts)
{
    m_contacts = contacts;
}

/*
    Sets the license information of \a type to \a value.
*/
void LicenseInfo::setLicenseInfo(LicenseInfoType type, const std::string &value)
{
    m_licenseInfo[type] = value;
}

/*
    Sets the possible consumers list of this license to \a consumers.
*/
void LicenseInfo::setConsumers(const std::vector<ConsumerInfo> &consumers)
{
    m_consumers = consumers;
}

/*
    Sets the raw JSON data of this license information to \a json.
*/
void LicenseInfo::setJsonData(const std::string &json)
{
    m_jsonData = json;
}

/*
    Clears current license information.
*/
void LicenseInfo::clear()
{
    m_licensee = LicenseeInfo();
    m_contacts = LicenseeContacts();
    m_licenseInfo.clear();
}

/*
    \class LicenseReservationInfo

    \brief The LicenseReservationInfo class provides methods for retrieving information about
           a reservation and the license the reservation is attached to.
*/

/*
    Constructs a new empty reservation information.
*/
LicenseReservationInfo::LicenseReservationInfo()
    : LicenseInfo()
    , m_status(StatusCode::UnknownError)
{
}

/*
    Clears current reservation information.
*/
void LicenseReservationInfo::clear()
{
    m_message.clear();
    m_status = StatusCode::UnknownError;
    m_features.clear();

    LicenseInfo::clear();
}

/*
    Returns the features relevant to the license consumer that performed the reservation.
*/
const std::map<std::string, std::string> &LicenseReservationInfo::licenseFeatures() const
{
    return m_features;
}

/*
    Returns the message retrieved from the license server when performing the reservation.
*/
std::string LicenseReservationInfo::message() const
{
    return m_message;
}

/*
    Returns the status code retrieved from the license server when performing the reservation.
*/
StatusCode LicenseReservationInfo::status() const
{
    return m_status;
}

/*
    Returns the name of the \a status code retrieved from the license service as a string.
*/
std::string LicenseReservationInfo::statusToString(StatusCode status) const
{
    switch (status) {
    case StatusCode::UnknownError:
        return "UnknownError";
    case StatusCode::Success:
        return "Success";
    case StatusCode::LicenseRejected:
        return "LicenseRejected";
    case StatusCode::LicensePoolFull:
        return "LicensePoolFull";
    case StatusCode::UnknownLicenseModel:
        return "UnknownLicenseModel";
    case StatusCode::UnknownReservationType:
        return "UnknownReservationType";
    case StatusCode::BadRequest:
        return "BadRequest";
    case StatusCode::InvalidResponse:
        return "InvalidResponse";
    case StatusCode::BadConnection:
        return "BadConnection";
    case StatusCode::Unauthorized:
        return "Unauthorized";
    case StatusCode::ServerError:
        return "ServerError";
    case StatusCode::ServerBusy:
        return "ServerBusy";
    case StatusCode::TimeMismatch:
        return "TimeMismatch";
    case StatusCode::TcpSocketError:
        return "TcpSocketError";
    case StatusCode::ServiceVersionTooLow:
        return "ServiceVersionTooLow";
    case StatusCode::ServiceVersionTooNew:
        return "ServiceVersionTooNew";
    case StatusCode::MissingServiceVersion:
        return "MissingServiceVersion";
    case StatusCode::SslVerifyError:
        return "SslVerifyError";
    case StatusCode::SslLocalCertificateError:
        return "SslLocalCertificateError";
    case StatusCode::ReservationNotFound:
        return "ReservationNotFound";
    case StatusCode::UnknownLicenseType:
        return "UnknownLicenseType";
    case StatusCode::BetterLicenseAvailable:
        return "BetterLicenseAvailable";
    case StatusCode::BadClientRequest:
        return "BadClientRequest";
    case StatusCode::RequestTimeout:
        return "RequestTimeout";
    case StatusCode::IncompatibleLicenseFormatVersion:
        return "IncompatibleLicenseFormatVersion";
    case StatusCode::TermsAndConditionsNotAccepted:
        return "TermsAndConditionsNotAccepted";
    case StatusCode::ServerHasNoLicenses:
        return "ServerHasNoLicenses";
    case StatusCode::SettingsError:
        return "SettingsError";
    default:
        return "Unknown";
    }
}

/*
    Sets the feature relevant to the license consumer that performed the reservation to \a features.
*/
void LicenseReservationInfo::setFeatures(const std::map<std::string, std::string> &features)
{
    m_features = features;
}

/*
    Sets the message retrieved from the license server to \a message.
*/
void LicenseReservationInfo::setMessage(const std::string &message)
{
    m_message = message;
}

/*
    Sets the status code of the reservation to \a status.
*/
void LicenseReservationInfo::setStatus(StatusCode status)
{
    m_status = status;
}

} // namespace QLicenseClient
