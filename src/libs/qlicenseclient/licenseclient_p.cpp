/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseclient_p.h"

#include "licenseresponseparser.h"
#include "lockfilelocker.h"
#include "licensecheckextension.h"

#include "commonsetup.h"
#include "constants.h"
#include "version.h"

#include <licdsetup.h>
#include <installationmanager.h>
#include <localqtaccount.h>
#include <utils.h>

#include <set>

using namespace QLicenseCore;

namespace QLicenseClient {

constexpr int sc_maxTries = 30;

static bool isKnownStatusCode(int code)
{
    // Note: update when adding new values to StatusCode
    static const std::set<int> sc_knownStatusCodes = {
        static_cast<int>(StatusCode::UnknownError),
        static_cast<int>(StatusCode::Success),
        static_cast<int>(StatusCode::LicenseRejected),
        static_cast<int>(StatusCode::LicensePoolFull),
        static_cast<int>(StatusCode::UnknownLicenseModel),
        static_cast<int>(StatusCode::UnknownReservationType),
        static_cast<int>(StatusCode::ReservationNotFound),
        static_cast<int>(StatusCode::UnknownLicenseType),
        static_cast<int>(StatusCode::BetterLicenseAvailable),
        static_cast<int>(StatusCode::TermsAndConditionsNotAccepted),
        static_cast<int>(StatusCode::ServerHasNoLicenses),
        static_cast<int>(StatusCode::BadRequest),
        static_cast<int>(StatusCode::InvalidResponse),
        static_cast<int>(StatusCode::BadConnection),
        static_cast<int>(StatusCode::Unauthorized),
        static_cast<int>(StatusCode::ServerError),
        static_cast<int>(StatusCode::ServerBusy),
        static_cast<int>(StatusCode::TimeMismatch),
        static_cast<int>(StatusCode::TcpSocketError),
        static_cast<int>(StatusCode::SslVerifyError),
        static_cast<int>(StatusCode::SslLocalCertificateError),
        static_cast<int>(StatusCode::BadClientRequest),
        static_cast<int>(StatusCode::RequestTimeout),
        static_cast<int>(StatusCode::SettingsError),
        static_cast<int>(StatusCode::ServiceVersionTooLow),
        static_cast<int>(StatusCode::ServiceVersionTooNew),
        static_cast<int>(StatusCode::MissingServiceVersion),
        static_cast<int>(StatusCode::IncompatibleLicenseFormatVersion),
    };

    return (sc_knownStatusCodes.find(code) != sc_knownStatusCodes.end());
}

static bool isSuccessStatusCode(int code)
{
    return code >= 0 && code < 100;
}

static std::string portFilePath()
{
    std::string portFilePath = LicdSetup::getQtAppDataLocation()
        + USER_SETTINGS_FOLDER_NAME + DIR_SEPARATOR + DAEMON_TCP_PORT_FILE;

    if (utils::fileExists(portFilePath))
        return portFilePath;

    // Maybe it's a system service instance...
    portFilePath = std::string(SYSTEM_WORK_DIR) + DAEMON_TCP_PORT_FILE;
    if (utils::fileExists(portFilePath))
        return portFilePath;

    return std::string();
}

static std::string lockFilePath()
{
    std::string lockFilePath = LicdSetup::getQtAppDataLocation()
        + USER_SETTINGS_FOLDER_NAME + DIR_SEPARATOR + DAEMON_LOCK_FILE;

    if (utils::fileExists(lockFilePath))
        return lockFilePath;

    lockFilePath = std::string(SYSTEM_WORK_DIR) + DAEMON_LOCK_FILE;
    if (utils::fileExists(lockFilePath))
        return lockFilePath;

    return std::string();
}

bool LicenseClientPrivate::serviceAvailable()
{
    // Valid port means the service is already running, no need to read the installation file
    uint16_t port = 0;
    if (getDaemonPort(port) && port > 0)
        return true;

    QLicenseCore::LockFile lock(LicdSetup::getQtAppDataLocation()
        + USER_SETTINGS_FOLDER_NAME + DIR_SEPARATOR + CIP_LOCK_FILE);
    LockFileLocker _(&lock);

    InstallationManager manager;
    manager.purgeObsoleteInstallations();

    const std::string program = manager.installationForVersion(DAEMON_VERSION);
    return !program.empty();
}

void LicenseClientPrivate::setMandatoryRequestParameters(std::stringstream &ss, const std::string &protocolVersion, const std::string &operation, const std::string &appName, const std::string &appVersion, const std::string &username)
{
    ss << " -pv " << protocolVersion;
    ss << " -o " << operation;
    ss << " -a " << appName;
    ss << " -v " << appVersion;
    ss << " -u " << username;
}

LicenseClientPrivate::LicenseClientPrivate(LicenseClient *qq)
    : q(qq)
    , m_reservationInfo(new LicenseReservationInfo)
    , m_lock(LicdSetup::getQtAppDataLocation()
        + USER_SETTINGS_FOLDER_NAME + DIR_SEPARATOR + CIP_LOCK_FILE)
{
    Logger::getInstance()->disableAll();

    if (Logger::clientPrintToStdoutFromEnv())
        Logger::getInstance()->setPrintToStdout(true);

    std::string logFilePath;
    if (Logger::clientPrintToFileFromEnv(logFilePath)) {
        Logger::getInstance()->setLogFile(logFilePath);
        Logger::getInstance()->setPrintToFile(true);
    }

    m_settings.init();
}

LicenseClientPrivate::~LicenseClientPrivate()
{
    release();
    delete m_reservationInfo;
}

bool LicenseClientPrivate::init(std::string &error, int timeoutSecs, bool isRetry)
{
    error.clear();

    if (m_tcpClient) {
        error = "Client already initialized";
        logError("%s", error.c_str());
        return false;
    }

    const std::string addr = DAEMON_DEFAULT_ADDR;
    uint16_t port = 0;
    if (!getDaemonPort(port) || port == 0) {
        // No port information, either service is not running or is currently starting up
        if (!m_lock.tryLock()) {
            // Some other instance is locking the file, handling the startup
            if (!waitServiceStart(port, error)) {
                m_lock.tryUnlock();
                logError("%s", error.c_str());
                return false;
            }
        } else {
            if (!startService(port, error)) {
                m_lock.tryUnlock();
                logError("%s", error.c_str());
                return false;
            }
        }
    }

    if (m_lock.isLocked() && !m_lock.tryUnlock()) {
        error = "Could not unlock lock file for client: " + m_lock.error();
        logError("%s", error.c_str());
        return false;
    }

    m_tcpClient.reset(new TcpClient(addr, port));
    std::string tcpError;
    if (!m_tcpClient->init(tcpError, timeoutSecs)) {
        m_tcpClient.reset(nullptr);

        error = "Could not initialize client socket: " + tcpError;

        if (port != 0) {
            // Found port information, but establishing socket connection failed.
            {
                // Test if any service process holds a lock file, it could be that the
                // socket connection failed due to some other error than invalid port.
                const std::string path = lockFilePath();
                if (!path.empty()) {
                    QLicenseCore::LockFile lockFile(path);
                    if (!lockFile.tryLock()) {
                        // Service is presumably running...
                        if (!isRetry) {
                            // If the port information was changed or the file deleted, some other
                            // client instance may be in the process of kick-starting the service
                            uint16_t newPort = 0;
                            if (!getDaemonPort(newPort) || (newPort > 0 && newPort != port)) {
                                logWarn("Could not connect to service port %d, but port information "
                                        "changed afterwards. Trying again..", port);
                                return init(error, timeoutSecs, true);
                            }
                        }

                        logError("Could not connect to service port %d, but service appears to be "
                                 "running. Giving up...", port);
                        return false;
                    }
                }
            }

            // No process holds the lock, remove the invalid port file to not block subsequent runs.
            logWarn("Service port %d appears to be invalid, removing old "
                    "information and trying again.", port);

            if (!isRetry && !removePortFile()) {
                error += ". Service port appears to be invalid, but removing the port file failed. "
                    "Please remove the file \"" + portFilePath() + "\" manually, and try again.";
                return false;
            }

            if (!isRetry)
                return init(error, timeoutSecs, true);
        }

        return false;
    }

    return true;
}

bool LicenseClientPrivate::reserve(const ClientProperties &clientProperties,
    LicenseClient::ReservationStatusCallback callback)
{
    std::string error;
    if (!m_tcpClient && !init(error)) {
        logError("%s", error.c_str());
        return false;
    }

    if (!utils::getCurrentUserName(m_requestParams.userId) || m_requestParams.userId.empty()) {
        logError("Not able to determine username");
        return false;
    }

    m_requestParams.consumerId = clientProperties.consumerId;
    m_requestParams.consumerVersion = clientProperties.consumerVersion;
    m_requestParams.consumerInstallationSchema = clientProperties.consumerInstallationSchema;
    const char* const delim = ",";
    std::ostringstream imploded;
    std::copy(clientProperties.consumerSupportedFeatures.begin(),
              clientProperties.consumerSupportedFeatures.end(), std::ostream_iterator<std::string> (imploded, delim));
    m_requestParams.consumerSupportedFeatures = imploded.str();
    m_requestParams.consumerProcessId = clientProperties.consumerProcessId.empty()
        ? utils::generateUuid()
        : clientProperties.consumerProcessId;
    m_requestParams.consumerBuildTimestamp = clientProperties.consumerBuildTimestamp;
    m_requestParams.protocolVersion = CIP_PROTOCOL_VERSION;

    // Get the JWT
    getJwt(m_jwt);
    utils::trimStr(m_jwt); // strip line feeds etc

    m_onReservationStatusChanged = callback;

    if (m_thread.joinable())
        m_thread.join();

    m_thread = std::thread([&]() {
        m_reservationInfo->clear();

        const bool success = requestLicense(LICENSE_REQUEST_CMD, m_jwt);
        m_onReservationStatusChanged(m_reservationInfo);

        if (success && m_reservationInfo->status() == StatusCode::Success)
            startWatchdog();
    });

    return true;
}

bool LicenseClientPrivate::parseLicenseResponse(QLicenseCore::ServiceResponse &response)
{
    m_reservationInfo->setJsonData(std::move(response.licenseInfo()));

    LicenseResponseParser parser(m_reservationInfo->jsonData());
    return parser.parse(m_reservationInfo);
}

bool LicenseClientPrivate::validateLicense()
{
    if (clientSetting(ClientSettings::sc_extendedLicenseCheck) != ClientSettings::sc_true)
        return true;

    LicenseCheckExtension lichek(m_reservationInfo);
    return lichek.verifyLicense();
}

StatusCode LicenseClientPrivate::statusCode(int status) const
{
    // Check the status code range - the daemon may respond with a code unknown to CIP
    if (isKnownStatusCode(status)) {
        // Known status code, use it directly
        return static_cast<StatusCode>(status);
    } else if (isSuccessStatusCode(status)) {
        // Unknown status code indicating success, use the default value for success
        return StatusCode::Success;
    } else {
        // Otherwise it's an unknown error
        logWarn("Received unknown error code: %d", status);
        return StatusCode::UnknownError;
    }
}

void LicenseClientPrivate::setClientSetting(const std::string &key, const std::string &value)
{
    std::string keyLowerCase = key;
    std::string valueLowerCase = value;

    utils::strToLower(keyLowerCase);
    utils::strToLower(valueLowerCase);

    m_settings.set(keyLowerCase, valueLowerCase);
}

std::string LicenseClientPrivate::clientSetting(const std::string &key) const
{
    std::string keyLowerCase = key;
    utils::strToLower(keyLowerCase);

    if (!m_settings.contains(keyLowerCase))
        return std::string();

    // values are stored lower case always
    return m_settings.get(keyLowerCase);
}

/*
    \internal

    Sends a request to the license daemon with \a operation as the license operation
    to perform, and parses the result.

    Returns \c false if the request could not be performed, or the response contains
    status indicating an error. Returns \c true otherwise
*/
bool LicenseClientPrivate::requestLicense(const std::string &operation, const std::string &jwt)
{
    std::unique_lock<std::mutex> _(m_mutex);

    if (!m_tcpClient) {
        setError(m_reservationInfo, "Client socket is disconnected", StatusCode::TcpSocketError);
        return false;
    }

    if (m_requestParams.consumerId.empty()) {
        setError(m_reservationInfo, "Application name cannot be empty", StatusCode::BadRequest);
        return false;
    }

    if (m_requestParams.consumerVersion.empty()) {
        setError(m_reservationInfo, "Application version cannot be empty", StatusCode::BadRequest);
        return false;
    }

    std::stringstream ss;
    setMandatoryRequestParameters(ss, m_requestParams.protocolVersion,
        operation, m_requestParams.consumerId, m_requestParams.consumerVersion, m_requestParams.userId);
    ss << " -c " << LIBLICENSECLIENT_VERSION;
    // Authentication
    if (!jwt.empty())
        ss << " -jwt " << jwt;
    // Consumer related
    if (!m_requestParams.consumerInstallationSchema.empty())
        ss << " -i " << m_requestParams.consumerInstallationSchema;
    if (!m_requestParams.consumerSupportedFeatures.empty())
        ss << " -f " << m_requestParams.consumerSupportedFeatures;
    if (!m_requestParams.consumerProcessId.empty())
        ss << " -pid " << m_requestParams.consumerProcessId;
    if (!m_requestParams.consumerBuildTimestamp.empty())
        ss << " -bts " << m_requestParams.consumerBuildTimestamp;

    std::string reply;
    std::string error;
    if (!sendReceive(ss.str(), reply, error)) {
        setError(m_reservationInfo, "Failed to send data: " + error, StatusCode::TcpSocketError);
        return false;
    }

    utils::trimStr(reply);

    QLicenseCore::ServiceResponse response;
    response.fromString(reply);

    return (parseServiceResponse(response) && parseLicenseResponse(response) && validateLicense());
}

void LicenseClientPrivate::release()
{
    // No need to do anything if TCP client was not initialized
    if (!m_tcpClient)
        return;

    if (m_thread.joinable())
        m_thread.join();

    m_reservationStatusWatchdog.reset(nullptr);
    m_tcpClient.reset(nullptr);
    m_reservationInfo->clear();
}

bool LicenseClientPrivate::status(LicenseClient::StatusOperation operation, std::string &status)
{
    std::string operationCmd;
    if (operation == LicenseClient::StatusOperation::DaemonVersion) {
        operationCmd = DAEMON_VERSION_CMD;
    } else if (operation == LicenseClient::StatusOperation::ServerVersion) {
        operationCmd = SERVER_VERSION_CMD;
    } else if (operation == LicenseClient::StatusOperation::Reservations) {
        operationCmd = RESERVATION_QUERY_CMD;
    }

    return requestOperation(operationCmd, status);
}

bool LicenseClientPrivate::getServiceSetting(const std::string &key, std::string &status)
{
    return requestOperation(GET_SERVICE_SETTING_CMD, status, key);
}

bool LicenseClientPrivate::setServiceSetting(const std::string &key, const std::string &value, const LicenseClient::SettingsType &settingsType, std::string &reply)
{
    return requestOperation(SET_SERVICE_SETTING_CMD, reply, key, value, settingsType);
}


bool LicenseClientPrivate::requestOperation(const std::string &operation, std::string &status,
    const std::string &key, const std::string &value, const LicenseClient::SettingsType &settingsType)
{
    if (!m_tcpClient && !init(status))
        return false;

    if (!utils::getCurrentUserName(m_requestParams.userId)
            || m_requestParams.userId.empty()) {
        status = "Not able to determine username";
        logError("%s", status.c_str());
        return false;
    }

    std::stringstream ss;
    setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
        operation, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, m_requestParams.userId);
    ss << " -c " << LIBLICENSECLIENT_VERSION;

    if (!key.empty())
        ss << " -key " << key;

    if (!value.empty())
        ss << " -value " << value;

    if (operation == SET_SERVICE_SETTING_CMD)
        ss << " -settings-type " << static_cast<int>(settingsType);

    std::string reply;
    std::string error;
    if (!sendReceive(ss.str(), reply, error)) {
        status = "Failed to send data: " + error;
        logError("%s", status.c_str());
        return false;
    }

    QLicenseCore::ServiceResponse response;
    response.fromString(reply);

    if (!parseServiceResponse(response)) {
        status = m_reservationInfo->message();
        logError("%s", status.c_str());
        return false;
    }

    status = m_reservationInfo->message();
    return true;
}

/*
    Sets the \a error string as the message and \a code as status code for reservation \a info.
 */
void LicenseClientPrivate::setError(LicenseReservationInfo *info, const std::string &error, StatusCode code) const
{
    if (info) {
        info->setMessage(error);
        info->setStatus(code);
    }

    logError("%s", error.c_str());
}

/*
    Sends a \a message to the license daemon, and reads the reply message retrievable
    with \a reply.

    Returns \c true on success, \c false otherwise. Message describing the error can
    be retrieved with \a error.
 */
bool LicenseClientPrivate::sendReceive(const std::string &message, std::string &reply, std::string &error)
{
    if (!m_tcpClient) {
        error = "Client socket is disconnected";
        logError("%s", error.c_str());
        return false;
    }

    const bool result = m_tcpClient->sendReceive(message, reply);
    if (!result)
        error = reply;

    return result;
}

bool LicenseClientPrivate::parseServiceResponse(QLicenseCore::ServiceResponse &response)
{
    // TODO: parse "service_response.service_version"?
    m_reservationInfo->setMessage(response.message());
    m_reservationInfo->setStatus(statusCode(response.statusCode()));

    return (response.success() && m_reservationInfo->status() == StatusCode::Success);
}

/*
    Tries to find the port daemon sets up in its startup
*/
bool LicenseClientPrivate::getDaemonPort(uint16_t &port)
{
    const std::string portFile = portFilePath();
    if (portFile.empty()) {
        logDebug("Could not find service port file \"%s\"", portFile.c_str());
        return false;
    }

    std::string portStr;
#if _WIN32
    if (!utils::readFileWin32(portStr, portFile) || portStr.empty()) {
#else
    if (!utils::readFile(portStr, portFile) || portStr.empty()) {
#endif
        logDebug("Could not read service port file \"%s\"", portFile.c_str());
        return false;
    }

    utils::trimStr(portStr);
    port = utils::strToInt(portStr);
    return true;
}

/*
    Finds out the JWT token
*/

bool LicenseClientPrivate::getJwt(std::string &jwt)
{
    LocalQtAccount account;
    if (!account.readFromDisk() || !account.loggedIn())
        return false;

    jwt = account.jwt();
    return true;
}

bool LicenseClientPrivate::startService(uint16_t &port, std::string &error)
{
    logInfo("License service not running, attempting startup");
    // locate service installation, the client (this) version should not exceed the service version
    InstallationManager manager;
    const std::string program = manager.installationForVersion(DAEMON_VERSION);
    if (program.empty()) {
        error = "Could not find license service installation matching the criteria for version \""
                + std::string(DAEMON_VERSION) + "\". The service version may be too old, or there is "
                "no installation registered in \"" + manager.configFilePath() + "\".";
        logError("%s", error.c_str());
        return false;
    }

    // Start the service in on-demand mode
    const std::vector<std::string> args = {"--mode", "on-demand", "--nostdout"};
    if (!utils::startDetached(program, args)) {
        error = "Could not start service process.";
        logError("%s", error.c_str());
        return false;
    }

    int count = 0;
    // give service some time to start up the TCP server...
    while (!getDaemonPort(port) && count < sc_maxTries) {
        utils::sleepSecs(1);
        ++count;
    }

    if (count == sc_maxTries) {
        error = "Not able to get the TCP port for license service";
        logError("%s", error.c_str());
        return false;
    }

    return true;
}

bool LicenseClientPrivate::waitServiceStart(uint16_t &port, std::string &error)
{
    logInfo("License service is being started up by another process, waiting...");

    int count = 0;
    while (!m_lock.tryLock() && count < sc_maxTries) {
        utils::sleepSecs(1);
        ++count;
    }

    if (!m_lock.isLocked()) {
        error = "Could not acquire lock file for client. Some other process might be using it.";
        logError("%s", error.c_str());
        return false;
    }

    // We can now get the port
    if (!getDaemonPort(port)) {
        error = "Not able to get the TCP port for license service";
        logError("%s", error.c_str());
        return false;
    }

    return true;
}

void LicenseClientPrivate::startWatchdog()
{
    if (m_onReservationStatusChanged) {
        m_reservationStatusWatchdog.reset(new Watchdog(std::chrono::seconds(CIP_HEARTBEAT_SECS)));
        m_reservationStatusWatchdog->setCallback([this] {
            m_reservationInfo->clear();

            const bool success = requestLicense(LICENSE_REQUEST_CMD, m_jwt);
            m_onReservationStatusChanged(m_reservationInfo);

            if (success && m_reservationInfo->status() == StatusCode::Success)
                m_reservationStatusWatchdog->reset();
        });

        m_reservationStatusWatchdog->start();
    }
}

bool LicenseClientPrivate::removePortFile()
{
    const std::string portFile = portFilePath();
    if (portFile.empty()) {
        logDebug("Skipping removal of port file with empty path");
        return true;
    }
    if (!utils::fileExists(portFile))
        return true;

    if (!utils::deleteFile(portFile)) {
        logError("Could not remove port file: %s", portFile.c_str());
        return false;
    }
    return true;
}

} // namespace QLicenseClient
