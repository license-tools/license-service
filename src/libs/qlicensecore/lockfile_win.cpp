/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "lockfile.h"

#include "utils.h"
#include "logger.h"

namespace QLicenseCore {

LockFile::LockFile(const std::string &filename)
    : m_filename(filename)
    , m_handle(0)
    , m_bytesWritten(0)
    , m_overlapped({0})
    , m_locked(false)
{
}

LockFile::~LockFile()
{
    if (!tryUnlock())
        logError("Could not unlock lock file %s: %s", m_filename.c_str(), m_error.c_str());
}

bool LockFile::tryLock(const std::string &fileData)
{
    m_error.clear();
    if (m_locked)
        return m_locked;

    std::wstring fullPath = utils::widen(m_filename);
    std::wstring directoryPath = fullPath.substr(0, fullPath.find_last_of(L"\\/"));

    if (!CreateDirectoryW(directoryPath.c_str(), NULL)) {
        if (GetLastError() != ERROR_ALREADY_EXISTS) {
            m_error = "Cannot create directory for lock file \"" + m_filename + "\": " + utils::getLastWin32Error();
            return false;
        }
    }

    m_handle = CreateFileW(utils::widen(m_filename).c_str(),
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ,
        NULL,
        utils::fileExists(m_filename) ? OPEN_EXISTING : CREATE_NEW,
        FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE,
        NULL
    );

    if (m_handle == INVALID_HANDLE_VALUE) {
        m_error = "Cannot open lock file \"" + m_filename + "\": " + utils::getLastWin32Error();
        return false;
    }

    const uint64_t pid = utils::getPID();
    const std::string data = fileData.empty() ? std::to_string(pid) : fileData;
    // We need to write the data first, as shared lock denies all processes write
    // access to the specified byte range of the file, including the process that
    // first locks the byte range.
    if (!WriteFile(m_handle, data.c_str(), data.size(), &m_bytesWritten, NULL)) {
        m_error = "Cannot write to lock file \"" + m_filename + "\": " + utils::getLastWin32Error();
        return false;
    }
    FlushFileBuffers(m_handle);

    if (!::LockFileEx(m_handle, LOCKFILE_FAIL_IMMEDIATELY, 0, MAXDWORD, MAXDWORD, &m_overlapped))
        m_error = "Cannot apply lock \"" + m_filename + "\": " + utils::getLastWin32Error();
    else
        m_locked = true;

    return m_locked;
}

bool LockFile::tryUnlock()
{
    m_error.clear();
    if (!m_locked)
        return true;

    if (!UnlockFileEx(m_handle, 0, MAXDWORD, MAXDWORD, &m_overlapped)) {
        m_error = "Cannot remove lock \"" + m_filename + "\": " + utils::getLastWin32Error();
    } else {
        m_locked = false;
        CloseHandle(m_handle);
    }

    return !m_locked;
}

bool LockFile::isLocked() const
{
    return m_locked;
}

std::string LockFile::error() const
{
    return m_error;
}

std::string LockFile::filename() const
{
    return m_filename;
}

} // namespace QLicenseCore
