/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "response.h"

#include "logger.h"
#include "jsonhandler.h"

#include <nlohmann/json.hpp>

namespace QLicenseCore {

ServiceResponse::ServiceResponse(bool success, const std::string &message, int code)
    : m_success(success)
    , m_message(message)
    , m_code(code)
{
}

std::string ServiceResponse::toString() const
{
    nlohmann::json json = {
        {"service_response", {
            {"service_version", DAEMON_VERSION},
            {"status", {
                {"success", m_success},
                {"message", m_message},
                {"code", m_code}
            }}
        }}
    };

    try {
        if (!m_licenseInfo.empty())
            json["license_response"] = nlohmann::json::parse(m_licenseInfo);
    } catch (const nlohmann::json::exception& e) {
        logError("Error constructing response JSON: ", e.what());
    }

    return json.dump(-1, ' ', false, nlohmann::json::error_handler_t::replace);
}

void ServiceResponse::fromString(const std::string &response)
{
    JsonHandler resp(response);
    resp.get("service_response.service_version", m_version);
    resp.get("service_response.status.message", m_message);
    resp.get("service_response.status.code", m_code);
    resp.get("service_response.status.success", m_success);

    nlohmann::json data = *resp.data();
    nlohmann::json licenseResponse = data["license_response"];
    m_licenseInfo = licenseResponse.dump(-1, ' ', false, nlohmann::json::error_handler_t::replace);
}

bool ServiceResponse::success() const
{
    return m_success;
}

void ServiceResponse::setSuccess(bool success)
{
    m_success = success;
}

std::string ServiceResponse::message() const
{
    return m_message;
}

void ServiceResponse::setMessage(const std::string &message)
{
    m_message = message;
}

int ServiceResponse::statusCode() const
{
    return m_code;
}

void ServiceResponse::setStatusCode(int code)
{
    m_code = code;
}

std::string ServiceResponse::version() const
{
    return m_version;
}

void ServiceResponse::setServiceResponse(bool success, const std::string &message, int code)
{
    m_success = success;
    m_message = message;
    m_code = code;
}

void ServiceResponse::setLicenseInfo(const std::string &license)
{
    m_licenseInfo = license;
}

std::string ServiceResponse::licenseInfo() const
{
    return m_licenseInfo;
}

} // namespace QLicenseCore
