/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
 */

#include "inifileparser.h"

#include "utils.h"
#include "logger.h"

#include <iostream>
#include <fstream>
#include <string>
#include <map>

namespace QLicenseCore {

bool IniFileParser::readKeyValue(const std::string &section, const std::string &key,
                                 std::string &value) const
{
    std::map<std::string, std::string> result;
    if (readSection(section, result)) {
        auto it = result.find(key);
        if (it != result.end()) {
            value = it->second;
            return true;
        }
    }

    return false;
}

template <typename MapType>
bool IniFileParser::readSection(const std::string &section, MapType &result) const
{
    std::map<std::string, MapType> results;
    if (readSections(results)) {
        result = results[section];
        return true;
    }

    return false;
}

template bool IniFileParser::readSection<std::map<std::string, std::string>>
    (const std::string &, std::map<std::string, std::string> &) const;
template bool IniFileParser::readSection<std::unordered_map<std::string, std::string>>
    (const std::string &, std::unordered_map<std::string, std::string> &) const;

template <typename MapType>
bool IniFileParser::readSections(std::map<std::string, MapType> &results) const
{
#if _WIN32
    std::ifstream file(utils::widen(m_filename));
#else
    std::ifstream file(m_filename);
#endif

    if (!file.is_open()) {
        logError("Unable to open file for reading: %s", m_filename.c_str());
        return false;
    }

    std::string line;
    std::string currentSection;
    while (std::getline(file, line)) {
        utils::trimStr(line);
        if (skipLine(line))
            continue;

        if (isSection(line)) {
            currentSection = line.substr(1, line.size() - 2);
            results[currentSection] = MapType();
            continue;
        } else {
            size_t pos = line.find('=');
            if (pos != std::string::npos) {
                results[currentSection][line.substr(0, pos)] = line.substr(pos + 1);
            }
        }
    }

    return true;
}

template bool IniFileParser::readSections<std::map<std::string, std::string>>
    (std::map<std::string, std::map<std::string, std::string>> &) const;
template bool IniFileParser::readSections<std::unordered_map<std::string, std::string>>
    (std::map<std::string, std::unordered_map<std::string, std::string>> &) const;

bool IniFileParser::writeKeyValue(const std::string &section,
                                  const std::map<std::string, std::string> &keyValuePairs) const
{
    std::map<std::string, std::string> modifiedKeyValuePairs = keyValuePairs;
#if _WIN32
    std::ifstream inputFile(utils::widen(m_filename));
    std::ofstream outputFile(utils::widen(m_filename + ".tmp"), std::ios::out | std::ios::trunc);
#else
    std::ifstream inputFile(m_filename);
    std::ofstream outputFile(m_filename + ".tmp", std::ios::out | std::ios::trunc);
#endif

    if (utils::fileExists(m_filename) && !inputFile.is_open()) {
        logError("Unable to write section '%s'. Can't open: %s", section.c_str(), m_filename.c_str());
        return false;
    }

    if (!outputFile.is_open()) {
        logError("Unable to write section '%s'. Can't open: %s.tmp", section.c_str(), m_filename.c_str());
        return false;
    }

    std::string line;
    bool sectionFound = false;
    bool sectionWritten = false;
    bool keyFound = false;
    while (std::getline(inputFile, line)) {
        utils::trimStr(line);
        keyFound = false;

        if (skipLine(line)) {
            outputFile << line << std::endl;
            continue;
        }

        if (line[0] == '[' && line.back() == ']') {
            if (line.substr(1, line.size() - 2) == section) {
                sectionFound = true;
                outputFile << line << std::endl;
                sectionWritten = true;
            } else {
                sectionFound = false;
                if (!modifiedKeyValuePairs.empty() && sectionWritten) {
                    writeKeyValuePairs(outputFile, modifiedKeyValuePairs);
                }
                outputFile << line << std::endl;
            }
        } else if (sectionFound) {
            size_t separatorPos = line.find('=');
            if (separatorPos != std::string::npos) {
                std::string currentKey = line.substr(0, separatorPos);
                auto it = modifiedKeyValuePairs.find(currentKey);
                if (it != modifiedKeyValuePairs.end()) {
                    keyFound = true;
                    outputFile << it->first << "=" << it->second << std::endl;
                    modifiedKeyValuePairs.erase(currentKey);
                }
            }
            if (!keyFound) {
                outputFile << line << std::endl;
            }
        } else {
            outputFile << line << std::endl;
        }
    }



    if (!sectionWritten) {
        // Section not found, so add it to the end of the file
        outputFile << "[" << section << "]" << std::endl;
        writeKeyValuePairs(outputFile, keyValuePairs);
    }

    inputFile.close();
    outputFile.close();
    std::remove(m_filename.c_str());
    std::rename((m_filename + ".tmp").c_str(), m_filename.c_str());
    return true;
}

bool IniFileParser::removeSection(const std::string &section) const
{
#if _WIN32
    std::ifstream inputFile(utils::widen(m_filename));
    std::ofstream outputFile(utils::widen(m_filename + ".tmp"), std::ios::out | std::ios::trunc);
#else
    std::ifstream inputFile(m_filename);
    std::ofstream outputFile(m_filename + ".tmp", std::ios::out | std::ios::trunc);
#endif

    if (utils::fileExists(m_filename) && !inputFile.is_open()) {
        logError("Unable to remove section '%s'. Can't open: %s", section.c_str(), m_filename.c_str());
        return false;
    }

    if (!outputFile.is_open()) {
        logError("Unable to remove section '%s'. Can't open: %s.tmp", section.c_str(), m_filename.c_str());
        return false;
    }

    std::string line;
    bool sectionErased = false;
    while (std::getline(inputFile, line)) {
        utils::trimStr(line);

        if (skipLine(line)) {
            outputFile << line << std::endl;
            continue;
        }

        if (line[0] == '[' && line.back() == ']') {
            if (line.substr(1, line.size() - 2) == section) {
                // Skip lines in the section
                while (std::getline(inputFile, line)) {
                    utils::trimStr(line);
                    if (line[0] == '[')
                        break;
                }

                sectionErased = true;
                outputFile << line << std::endl;
            } else {
                outputFile << line << std::endl;
            }
        } else {
            outputFile << line << std::endl;
        }
    }

    inputFile.close();
    outputFile.close();
    std::remove(m_filename.c_str());
    std::rename((m_filename + ".tmp").c_str(), m_filename.c_str());

    return sectionErased;
}

void IniFileParser::writeKeyValuePairs(
        std::ofstream &file, const std::map<std::string, std::string> &keyValuePairs) const
{
    // Write or update key-value pairs
    for (const auto &kv : keyValuePairs) {
        file << kv.first << "=" << kv.second << std::endl;
    }
}

bool IniFileParser::skipLine(const std::string &line) const
{
    return line.empty() || line[0] == ';' || line[0] == '#';
}

bool IniFileParser::isSection(const std::string &line) const
{
    return line[0] == '[' && line.back() == ']';
}

} // namespace QLicenseCore
