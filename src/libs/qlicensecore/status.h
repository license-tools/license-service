/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <string>

namespace QLicenseCore {

/*
    To-be kept in sync with QLicenseClient::StatusCode
*/
enum class Status {
    UNKNOWN_ERROR = -1,
    // Success codes       [0-99]
    SUCCESS = 0,
    // Rejection codes     [100-199]
    LICENSE_REJECTED = 100,
    LICENSE_POOL_FULL = 101,
    UNKNOWN_LICENSE_MODEL = 102,
    UNKNOWN_RESERVATION_TYPE = 103,
    RESERVATION_NOT_FOUND = 104,
    UNKNOWN_LICENSE_TYPE = 105,
    BETTER_LICENSE_AVAILABLE = 106,
    TERMS_AND_CONDITIONS_NOT_ACCEPTED = 107,
    SERVER_HAS_NO_LICENSES = 108,
    // Request error codes [200-299]
    BAD_REQUEST = 200,
    INVALID_RESPONSE = 201,
    BAD_CONNECTION = 202,
    UNAUTHORIZED = 203,
    SERVER_ERROR = 204,
    SERVER_BUSY = 205,
    TIME_MISMATCH = 206,
    TCP_SOCKET_ERROR = 207, // only used on client side
    SSL_VERIFY_ERROR = 208,
    SSL_LOCAL_CERTIFICATE_ERROR = 209,
    BAD_CLIENT_REQUEST = 210,
    REQUEST_TIMEOUT = 211,
    SETTINGS_ERROR = 212,
    // Version error codes [300-399]
    SERVICE_VERSION_TOO_LOW = 300,
    SERVICE_VERSION_TOO_NEW = 301,
    MISSING_SERVICE_VERSION = 302,
    INCOMPATIBLE_LICENSE_FORMAT_VERSION = 303,
    // Interal only codes
    NO_CLIENT_FOUND = 1000,
};

std::string statusString(Status status);
bool isNetworkError(Status status);

namespace ServerResponseCode {

Status codeToStatus(const std::string &code);
bool isSuccessCode(const std::string &code);

} // namespace ServerResponseCode

} // namespace QLicenseCore
