/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <stdexcept>
#include <string>

namespace QLicenseCore {

class Error : public std::runtime_error
{
public:
    explicit Error(const char *message)
        : std::runtime_error(message)
    {}

    explicit Error(const std::string &message)
        : std::runtime_error(message)
    {}

    virtual ~Error() noexcept
    {}

    virtual const char *what() const noexcept override
    {
        return std::runtime_error::what();
    }
};

} // namespace QLicenseCore
