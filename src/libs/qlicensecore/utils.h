/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include <sys/stat.h>
#include <sys/types.h>
#include <vector>
#include <chrono>
#include <ctime>
#include <cstring>
#include <cstdint>
#include <string>

#if _WIN32
    // Windows
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
    #include <stdint.h> // portable: uint64_t   MSVC: __int64
    #include <io.h>
    #include <iomanip>
    #include "../3rdparty/dirent/dirent.h"
#else
    // Linux or Mac
    #include <sys/time.h>
    #include <unistd.h>
    #include <dirent.h>
    #include <pwd.h>
    #include <grp.h>
#endif

namespace QLicenseCore { namespace utils {

/*
*   String tools
*/

void trimLeft(std::string &s);
void trimRight(std::string &s);
void trimStr(std::string &s);
std::vector<std::string> splitStr(const std::string &str, const char delimiter=' ');
std::string strToLower(const std::string &str);
std::string strToUpper(const std::string &str);
int strToInt(const std::string &str);
bool strToUint64(const std::string &input, uint64_t &output);
std::string generateRandomString(size_t length);
std::string replaceCharacters(const std::string &input, const std::string &charactersToReplace, const char &replacement);
bool constTimeCompare(const std::string &a, const std::string &b);
#if _WIN32
std::string narrow(const wchar_t *s);
std::wstring widen(const char *s);
std::string narrow(const std::wstring &s);
std::wstring widen(const std::string &s);
#endif

/*
*   Filesystem tools
*/
std::string getUserHomeDir();
std::string getTempDir();
bool writeToFile(const std::string &filepath, const std::string &data, bool append=false);
bool createDir(const std::string &filepath);
bool removeDir(const std::string &path);
bool readFile(std::string &str, const std::string &filepath);
#if _WIN32
bool readFileWin32(std::string &str, const std::string &filepath);
#endif
bool fileExists(const std::string &name);
std::string getFileOwnerName(const std::string &filename);
void getFileOwner(const std::string &filename, uint16_t &owner, uint16_t &group);
std::vector<std::string> getDirListing(const std::string &directory,
    const std::string &filter = "", int typeMask = DT_REG);
bool deleteFile(const std::string& filepath);
std::string getFilenameWithoutPath(std::string path);
std::string getFilenameWithoutExtension(std::string path);
std::string getPathWithoutFilename(std::string path);
std::string getProgramPath();
std::string appdataPath();
std::string workingDirectory();
bool setWorkingDirectory(const std::string &dir);

/*
*   Other
*/

uint16_t generateUniqueShortId();
int compareVersion(const std::string &lhs, const std::string &rhs, int maxParts = 0);
bool isVersionNumSeparator(const char &c);
bool isVersionNum(const std::string& str);

// Hashing
std::string generateUuid();
void update_adler32(unsigned long &chksum,const unsigned char *data, size_t len);
unsigned long adler32( const unsigned char *data, size_t len );

// Time utils
std::time_t utcTime();
std::time_t localTime();
std::chrono::system_clock::time_point utcTimeMs();

std::string localTimeToString(time_t epochTime, const char* format = "%Y-%m-%d %H:%M:%S");
time_t stringToLocalTime(const char* time, const char* format = "%Y-%m-%d %H:%M:%S");

std::string utcTimeToString(time_t utcTime, const char* format = "%Y-%m-%d %H:%M:%S");
time_t stringToUtcTime(const char* time, const char* format = "%Y-%m-%d %H:%M:%S");

std::string utcTimeMsToString(std::chrono::system_clock::time_point utcTime);
std::chrono::system_clock::time_point stringToUtcTimeMs(const std::string &time);

long getTimezoneOffset();


// System utils
bool getCurrentUserName(std::string &username);
bool getUsernameFromEnv(std::string &username);
bool getUsernameFromOS(std::string &username);
std::string getOsName();
std::string getOsArchitecture();
std::string getHostname();
int getHostWordSize();
uint64_t getPID();
void setBlockSignalsMask();
void setMaxOpenFileDescriptrors();
void sleepSecs(uint32_t timeInSecs);
void sleepMillisecs(uint32_t timeInMilliSecs);
bool startDetached(const std::string &program, const std::vector<std::string> &args);

#if _WIN32
std::string getLastWin32Error();
#endif

// Swap endian of any data
template <typename T> T swapEndian(T val)
{
    T retVal;
    char *pVal = (char*) &val;
    char *pRetVal = (char*)&retVal;
    int size = sizeof(T);
    for (int i=0; i<size; i++) {
        pRetVal[size-1-i] = pVal[i];
    }
    return retVal;
}
/*
*   App-specific utils
*/

std::string stripNonAscii(const std::string &str);

} } // namespace QLicenseCoree::utils
