/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "logger.h"

#include "commonsetup.h"
#include "utils.h"

#include <iostream>
#include <sstream>

#if _WIN32
#include <stdio.h>
#include <io.h>
#else
#include <unistd.h>
#endif

namespace QLicenseCore {

Logger::Logger() :
    m_logLevel(LogLevel::LOGLEVEL_INFO),
    m_printToFile(true),
    m_printToStdout(true)
{
}

Logger::~Logger()
{
    if (m_logStream.is_open())
        m_logStream.close();
}

Logger *Logger::getInstance()
{
    static Logger instance;
    return &instance;
}

void Logger::setLogLevel(LogLevel level)
{
    if (level < LogLevel::LOGLEVEL_NONE
            || level > LogLevel::LOGLEVEL_SILLY) {
        warn("Logger", "Not a valid loglevel %d", (int)level);
    }
    m_logLevel = level;
    info("Logger", "Log level is set to %s", logLevelAsCString());
}

LogLevel Logger::logLevel() const
{
    return m_logLevel;
}

void Logger::setPrintToStdout(bool on_off)
{
    m_printToStdout = on_off;
    info("Logger", "Printing to stdout is %s", (on_off ? "enabled" : "disabled"));
}

void Logger::setPrintToFile(bool on_off)
{
    m_printToFile = on_off;
    info("Logger", "Printing to file is %s", (on_off ? "enabled" : "disabled"));
}

void Logger::disableAll() {
    m_printToFile = false;
    m_printToStdout = false;
}

void Logger::error(const std::string &file, const char *msg, ...)
{
    if (m_logLevel >= LogLevel::LOGLEVEL_ERROR) {
        std::string inputStr;
        HANDLE_VARGS(inputStr, msg);
        std::stringstream out;
        out << utils::localTimeToString(utils::localTime()) << " [ERROR] [" << file << "]: ";
        out << inputStr;
        print(out.str());
    }
}

void Logger::warn(const std::string &file, const char *msg, ...)
{
    if (m_logLevel >= LogLevel::LOGLEVEL_WARNING) {
        std::string inputStr;
        HANDLE_VARGS(inputStr, msg);
        std::stringstream out;
        out << utils::localTimeToString(utils::localTime()) << " [WARNING] [" << file << "]: ";
        out << inputStr;
        print(out.str());
    }
}

void Logger::info(const std::string &file, const char *msg, ...)
{
    if (m_logLevel >= LogLevel::LOGLEVEL_INFO) {
        std::string inputStr;
        HANDLE_VARGS( inputStr, msg);
        std::stringstream out;
        out << utils::localTimeToString(utils::localTime()) << " [INFO] [" << file << "]: ";
        out << inputStr;
        print(out.str());
    }
}

void Logger::debug(const std::string &file, const char *function, int line, const char *msg, ...)
{

    if (m_logLevel >= LogLevel::LOGLEVEL_DEBUG)
    {
        std::string filename = utils::getFilenameWithoutPath(std::string(file));
        std::stringstream out;
        if (ENABLE_DEBUG_LOG == true) {
            out << utils::localTimeToString(utils::localTime()) << " [DEBUG] [" << filename << "]: "
                        << ", in Func: " << function << ", at line " << line;
        } else {
            out << utils::localTimeToString(utils::localTime()) << " [DEBUG] [" << filename << "]: ";

        }
        std::string inputStr;
        HANDLE_VARGS(inputStr, msg);
        std::string output = out.str() + inputStr;
        print(output);
    } else {
        // Suppress the compiler warning for unused variables
        (void)file;
        (void)function;
        (void)line;
        (void)msg;
    }
}

void Logger::silly(const std::string &file, const char *function, int line, const char *msg, ...)
{
    if (m_logLevel >= LogLevel::LOGLEVEL_SILLY)  {
        const std::string filename = utils::getFilenameWithoutPath(std::string(file));
        std::stringstream out;
        if (ENABLE_DEBUG_LOG == true) {
            out << utils::localTimeToString(utils::localTime()) << " [SILLY] [" << filename << "]: "
                << ", in Func: " << function << ", at line " << line;
        } else {
            out << utils::localTimeToString(utils::localTime()) << " [SILLY] [" << filename << "]: ";
        }
        std::string inputStr;
        HANDLE_VARGS(inputStr, msg);
        const std::string output = out.str() + inputStr;
        print(output);
    } else {
        // Suppress the compiler warning for unused variables
        (void)file;
        (void)function;
        (void)line;
        (void)msg;
    }
}

int Logger::print(std::string text)
{
    m_mutex.lock();
    if (m_printToStdout) {
        std::cout << text << std::endl;
    }
    if (m_printToFile && !m_logFile.empty()) {
        if (!m_logStream.is_open()) {
#if _WIN32
            m_logStream.open(utils::widen(m_logFile), std::ios::app);
#else
            m_logStream.open(m_logFile, std::ios::app);
#endif
        }
        if (m_logStream.fail()) {
            std::cout << "Failed to open log file '" << m_logFile << "'" << std::endl;
            m_logStream.close();
            m_mutex.unlock();
            return -1;
        }
        m_logStream << text << std::endl;;
    }
    m_mutex.unlock();
    return 0;
}


const char *Logger::logLevelAsCString()
{
    switch (m_logLevel) {
        case LogLevel::LOGLEVEL_NONE :
            return "NONE";
        case LogLevel::LOGLEVEL_ERROR :
            return "ERROR";
        case LogLevel::LOGLEVEL_WARNING :
            return "WARNING";
        case LogLevel::LOGLEVEL_INFO :
            return "INFO";
        case LogLevel::LOGLEVEL_DEBUG :
            return "DEBUG";
        case LogLevel::LOGLEVEL_SILLY :
            return "SILLY";
        default :
            return "Unknown";
    }
}

void Logger::setLogFile(std::string logFile)
{
    if (m_logStream.is_open())
        m_logStream.close();

    m_logFile = logFile;
}

bool Logger::clientPrintToStdoutFromEnv()
{
    const char *value = std::getenv("QTLICD_CLIENT_LOG_TO_STDOUT");
    if (!value)
        return false;

    const std::string valueStr = QLicenseCore::utils::strToLower(value);
    if (valueStr == "1" || valueStr == "true" || valueStr == "yes")
        return true;
    else if (valueStr == "0" || valueStr == "false" || valueStr == "no")
        return false;

    // default if unrecognized value
    return false;
}

bool Logger::clientPrintToFileFromEnv(std::string &filename)
{
    const char *value = std::getenv("QTLICD_CLIENT_LOG_TO_FILE");
    if (!value)
        return false;

    filename = std::string(value);
    return !filename.empty();
}

bool Logger::outputRedirected()
{
#if _WIN32
    return !_isatty(_fileno(stdout));
#else
    return !isatty(fileno(stdout));
#endif
}

} // namespace QLicenseCore
