/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "pipe.h"
#include "logger.h"

#include <chrono>

namespace QLicenseCore {

static constexpr const char sc_partDelimiter = ':';
static constexpr const char sc_msgDelimiter = ';';

Pipe::Pipe()
{
}

Pipe::~Pipe()
{
}

void Pipe::write(const std::string &message, Type type, const std::string &identifier)
{
    if (message.empty()) {
        logWarn("Message is empty. Type: %d  Identifier: %s", static_cast<int>(type), identifier.c_str());
        return;
    }
    if (message.find(sc_partDelimiter) != std::string::npos) {
        logWarn("Message: \"%s\" contains illegal character: \"%s\".", message.c_str(), sc_partDelimiter);
        return;
    }
    if (message.find(sc_msgDelimiter) != std::string::npos) {
        logWarn("Message: \"%s\" contains illegal character: \"%s\".", message.c_str(), sc_msgDelimiter);
        return;
    }

    Message msg;
    msg.type = type;
    msg.identifier = identifier;
    msg.message = message;

    {
        const std::lock_guard<std::mutex> _(m_mutex);
        m_messages.push_back(std::move(msg));
        m_messageCount = m_messages.size();
    }

    m_cv.notify_one();
}

std::string Pipe::read(Type *type, std::string *identifier)
{
    if (!m_messageCount)
        return std::string();

    const std::lock_guard<std::mutex> _(m_mutex);
    const Message msg = m_messages.front();
    m_messages.pop_front();
    m_messageCount = m_messages.size();

    if (type)
        *type = msg.type;
    if (identifier)
        *identifier = msg.identifier;

    return msg.message;
}

bool Pipe::waitForReadyRead(int timeoutSecs)
{
    if (m_messageCount > 0)
        return true;

    std::unique_lock<std::mutex> lock(m_mutex);
    if (timeoutSecs == -1) {
        m_cv.wait(lock, [&]() {
            return m_messageCount > 0;
        });
    } else {
        bool readyRead = m_cv.wait_for(lock, std::chrono::seconds(timeoutSecs), [&]() {
            return m_messageCount > 0;
        });
        return readyRead;
    }
    return true;
}

int Pipe::queuedMessagesCount() const
{
    return m_messageCount;
}

Pipe *Pipe::globalObject()
{
    static Pipe instance;
    return &instance;
}

} // namespace QLicenseCore
