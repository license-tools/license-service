/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "settings.h"
#include "inifileparser.h"

namespace QLicenseCore {

class QtAccountSetup : public Settings
{
public:
    explicit QtAccountSetup(const std::string &settingsFilePath);

    bool init() override;

public:
    static bool createSettingsFile(const std::string &path, const std::string &email,
                                   const std::string &jwt);

private:
    bool getQtAccountInfo();

private:
    std::string m_settingsFilePath;
    IniFileParser m_parser;
};

} // namespace QLicenseCore
