/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "httpclient.h"

#include "constants.h"
#include "version.h"
#include "commonsetup.h"

#include <logger.h>
#include <pipe.h>
#include <utils.h>

#include <sstream>

using namespace QLicenseCore;

namespace QLicenseService {

HttpResult::HttpResult(HttpRequest *request)
    : m_request(request)
    , m_deleteRequest(false)
    , m_code(-1)
{
}

HttpResult::~HttpResult()
{
    if (m_deleteRequest && m_request)
        delete m_request;
}

HttpRequest *HttpResult::request() const
{
    return m_request;
}

HttpResponse HttpResult::response() const
{
    HttpResponse response;
    response.body = m_reply;
    response.headers = parseHeaders(m_headers);
    response.code = m_code;

    return response;
}

long HttpResult::code() const
{
    return m_code;
}

CURLcode HttpResult::error() const
{
    return m_error;
}

std::string HttpResult::errorString() const
{
    return curl_easy_strerror(m_error);
}

bool HttpResult::deleteRequest() const
{
    return m_deleteRequest;
}

void HttpResult::setDeleteRequest(bool which)
{
    m_deleteRequest = which;
}

std::map<std::string, std::string> HttpResult::parseHeaders(const std::string &headers) const
{
    std::map<std::string, std::string> headersMap;
    std::istringstream iss(headers);

    std::string line;
    while (std::getline(iss, line) && !line.empty()) {
        const size_t colonPos = line.find(':');
        if (colonPos != std::string::npos) {
            std::string name = line.substr(0, colonPos);
            std::string value = line.substr(colonPos + 1);
            utils::trimStr(name);
            utils::trimStr(value);
            headersMap[name] = value;
        }
    }

    return std::move(headersMap);
}


HttpRequest::HttpRequest(const std::string &url, const std::string &authKey)
    : m_url(url)
    , m_delay(0)
    , m_forceHttps(false)
    , m_requestTimeout(60)
    , m_certRevokeBehavior(CertRevokeOption::Default)
    , m_headers(nullptr)
{
    if (!authKey.empty()) {
        // Add auth key in headers
        const std::string auth = "Authorization: " + authKey;
        m_headers = curl_slist_append(m_headers, auth.c_str());
    }

    std::string userAgent = "User-Agent: License daemon /";
    userAgent =+ DAEMON_VERSION;

    m_headers = curl_slist_append(m_headers, userAgent.c_str());
    m_headers = curl_slist_append(m_headers, "Accept: */*");
    m_headers = curl_slist_append(m_headers, "Content-Type: application/json");
    m_headers = curl_slist_append(m_headers, "charset: utf-8");

    // init the curl session
    m_curl = curl_easy_init();
}

HttpRequest::~HttpRequest()
{
    // cleanup curl stuff
    curl_easy_cleanup(m_curl);
    curl_slist_free_all(m_headers);
}

HttpResult *HttpRequest::doRequest(const std::string &payload)
{
    if (m_delay > 0) {
        logDebug("Waiting %d msecs before performing request.", m_delay);
        utils::sleepMillisecs(m_delay);
    }

    utils::setBlockSignalsMask();

    HttpResult *result = new HttpResult(this);

    if (Logger::getInstance()->logLevel() >= LogLevel::LOGLEVEL_SILLY) {
        curl_easy_setopt(m_curl, CURLOPT_DEBUGFUNCTION, debugCallback);
        // the DEBUGFUNCTION has no effect until we enable VERBOSE
        curl_easy_setopt(m_curl, CURLOPT_VERBOSE, 1);
    }

    // Set all received data to be send to our callback function
    curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, writeCallback);
    curl_easy_setopt(m_curl, CURLOPT_HEADERFUNCTION, writeCallback);

    // Pass the buffer to a callback
    curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &result->m_reply);
    curl_easy_setopt(m_curl, CURLOPT_HEADERDATA, &result->m_headers);

    // Set the URL / headers
    curl_easy_setopt(m_curl, CURLOPT_URL, m_url.c_str());
    curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, m_headers);

    if (m_forceHttps)
        curl_easy_setopt(m_curl, CURLOPT_PROTOCOLS_STR, "https");

    if (!m_caBundlePath.empty())
        curl_easy_setopt(m_curl, CURLOPT_CAINFO, m_caBundlePath.c_str());

    curl_easy_setopt(m_curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
#ifdef _WIN32
    // Only supported for Schannel
    if (m_certRevokeBehavior == CertRevokeOption::BestEffort) {
        // Ignore certificate revocation checks in case of missing or offline distribution points.
        curl_easy_setopt(m_curl, CURLOPT_SSL_OPTIONS, (long)CURLSSLOPT_REVOKE_BEST_EFFORT);
    } else if (m_certRevokeBehavior == CertRevokeOption::NoRevoke) {
        // Disable certificate revocation checks entirely
        curl_easy_setopt(m_curl, CURLOPT_SSL_OPTIONS, (long)CURLSSLOPT_NO_REVOKE);
    } // else use default SSL behavior bitmask
#endif

    if (!payload.empty()) {
        logDebug("POST to URL: %s", m_url.c_str() );
        logDebug("Payload to send:\n%s", payload.c_str());
        curl_easy_setopt(m_curl, CURLOPT_HTTPPOST, 1);
        curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, payload.c_str());
    } else {
        logDebug("GET to URL: %s", m_url.c_str() );
        curl_easy_setopt(m_curl, CURLOPT_HTTPGET, 1); // for server ping
    }

    if (curl_easy_setopt(m_curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL) != CURLE_OK) {
        logWarn("No SSL support available");
    }

    curl_easy_setopt(m_curl, CURLOPT_TIMEOUT, m_requestTimeout);

    // get it
    result->m_error = curl_easy_perform(m_curl);
    curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &result->m_code);

    // check for errors
    if (result->m_error != CURLE_OK) {
        logError("HTTP transfer failed, URL: %s: %s", m_url.c_str(), curl_easy_strerror(result->m_error));
        if (onError)
            onError(curl_easy_strerror(result->m_error));

        return result;
    }

    logDebug("[%d] %d bytes retrieved from license server:\n%s",
        result->code(), result->m_reply.length(), result->m_reply.c_str());

    if (onResultReady)
        onResultReady(result->m_reply);

    return result;
}

/*
    Sets \a msecs to sleep before performing the request. Can be used to add wait time
    for retried requests instead of performing immediately.
*/
void HttpRequest::setDelay(uint32_t msecs)
{
    m_delay = msecs;
}

void HttpRequest::setForceHttps(bool https)
{
    m_forceHttps = https;
}

void HttpRequest::setCaBundlePath(const std::string &path)
{
    m_caBundlePath = path;
}

void HttpRequest::setRequestTimeout(long timeoutSecs)
{
    m_requestTimeout = timeoutSecs;
}

void HttpRequest::setCertRevokeBehavior(CertRevokeOption option)
{
    m_certRevokeBehavior = option;
}

size_t HttpRequest::writeCallback(char *contents, size_t size, size_t nmemb, void *userp)
{
    //std::cout << "Reading data\n";
    ((std::string *)userp)->append(contents, size * nmemb);
    return size * nmemb;
}

int HttpRequest::debugCallback(CURL *handle, curl_infotype type, char *data, size_t size, void *userp)
{
    (void)handle; // suppress compiler warnings
    (void)userp;

    std::string infoType;

    // Skip SSL/TLS (binary) data: CURLINFO_SSL_DATA_IN and CURLINFO_SSL_DATA_OUT
    switch (type) {
    case CURLINFO_TEXT:
        infoType = "== Info";
        break;
    case CURLINFO_HEADER_OUT:
        infoType = "=> Send header";
        break;
    case CURLINFO_DATA_OUT:
        infoType = "=> Send data";
        break;
    case CURLINFO_HEADER_IN:
        infoType = "<= Recv header";
        break;
    case CURLINFO_DATA_IN:
        infoType = "<= Recv data";
        break;
    default:
        return 0;
    }

    std::string dataOut;
    dataOut.assign(data, size);

    if (!dataOut.empty() && dataOut.back() == '\n') {
        // remove the last newline as logger adds one automatically
        dataOut.pop_back();
    }

    logSilly("%s: %s", infoType.c_str(), dataOut.c_str());
    return 0;
}


HttpClient::HttpClient(const std::string &serverUrl)
    : m_forceHttps(false)
    , m_requestTimeout(-1)
    , m_certRevokeBehavior(HttpRequest::CertRevokeOption::Default)
    , m_serverUrl(serverUrl)
{
    curl_global_init(CURL_GLOBAL_ALL);
}

HttpClient::~HttpClient()
{
    for (auto &item : m_futures) {
        auto &future = item.second;
        if (!future.valid())
            continue;

        HttpResult *result = future.get();
        if (!result)
            continue;

        result->setDeleteRequest(true);
        delete result;
    }
    m_futures.clear();
    curl_global_cleanup();
}

void HttpClient::send(uint16_t &requestId, const std::string &payload, const std::string &accessPoint,
    const std::string &server, const std::string &authKey, uint32_t delay)
{
    // If server URL is not given as param, we use the default
    std::string requestUrl = server.empty() ? m_serverUrl : server;
    requestUrl += accessPoint;

    auto sendFinished = [requestId](const std::string &message) {
        Pipe::globalObject()->write(PIPE_MSG_READY_READ, Pipe::Type::HttpClient, std::to_string(requestId));
    };

    HttpRequest *request = new HttpRequest(requestUrl, authKey);
    request->setDelay(delay);
    request->setForceHttps(m_forceHttps);
    request->setCaBundlePath(m_caBundlePath);
    request->setCertRevokeBehavior(m_certRevokeBehavior);
    if (m_requestTimeout >= 0)
        request->setRequestTimeout(m_requestTimeout);

    request->onResultReady = sendFinished;
    request->onError = sendFinished;

    m_futures.insert(std::make_pair<const uint16_t &, std::future<HttpResult *>>
        (requestId, std::async(std::launch::async, &HttpRequest::doRequest, request, payload)));
}

bool HttpClient::sendReceive(HttpResponse &response, const std::string &payload,
    const std::string &accessPoint, const std::string &server, const std::string &authKey)
{
    // If server URL is not given as param, we use the default
    std::string requestUrl = server.empty() ? m_serverUrl : server;
    requestUrl += accessPoint;

    HttpRequest request(requestUrl, authKey);
    request.setForceHttps(m_forceHttps);
    request.setCaBundlePath(m_caBundlePath);
    request.setCertRevokeBehavior(m_certRevokeBehavior);
    if (m_requestTimeout >= 0)
        request.setRequestTimeout(m_requestTimeout);

    std::unique_ptr<HttpResult> result(request.doRequest(payload));

    if (result->error() != CURLE_OK) {
        return false;
    }
    response = std::move(result->response());
    return true;
}

Status HttpClient::receive(uint16_t &clientId, HttpResponse &response)
{
    auto it = m_futures.find(clientId);
    if (it == m_futures.end()) {
        logWarn("No HTTP request result available with client id: %d", clientId);
        return Status::NO_CLIENT_FOUND;
    }

    auto future = std::move(it->second);
    m_futures.erase(it);

    if (!future.valid()) {
        logError("Invalid future object for HTTP request result");
        return Status::INVALID_RESPONSE;
    }

    std::unique_ptr<HttpResult> result(future.get());
    if (!result) {
        logError("Invalid result object for HTTP request");
        return Status::INVALID_RESPONSE;
    }

    result->setDeleteRequest(true);
    if (result->error() != CURLE_OK) {
        if (result->error() == CURLE_SSL_CONNECT_ERROR
                || result->error() == CURLE_PEER_FAILED_VERIFICATION
                || result->error() == CURLE_SSL_ISSUER_ERROR) {
            return Status::SSL_VERIFY_ERROR;
        } else if (result->error() == CURLE_SSL_CERTPROBLEM
                || result->error() == CURLE_SSL_CACERT_BADFILE
                || result->error() == CURLE_SSL_CLIENTCERT) {
            return Status::SSL_LOCAL_CERTIFICATE_ERROR;
        } else if (result->error() == CURLE_OPERATION_TIMEDOUT) {
            return Status::REQUEST_TIMEOUT;
        }
        // default
        return Status::BAD_CONNECTION;
    }

    response = std::move(result->response());
    return Status::SUCCESS;
}

void HttpClient::setServerUrl(const std::string &serverUrl)
{
    m_serverUrl = serverUrl;
}

void HttpClient::setForceHttps(bool https)
{
    m_forceHttps = https;
}

/*
    Set the absolute path to CA bundle or empty string to detect the path automatically.
    This has only effect on linux.
*/
void HttpClient::setCABundlePath(const std::string &path)
{
#if __linux__
    if (!path.empty()) {
        m_caBundlePath = path;
        logDebug("Manually set CA bundle: %s", path.c_str());
        return;
    }
    // else we search for known paths
    for (const auto &bundlePath : sc_caBundleSearchPaths) {
        if (utils::fileExists(bundlePath)) {
            logDebug("Found CA bundle: %s", bundlePath.c_str());
            m_caBundlePath = bundlePath;
            break;
        }
    }

    if (m_caBundlePath.empty()) {
        logWarn("CA bundle path was not set!");
        CURL *curl = curl_easy_init();
        if (curl) {
            char *caPath = nullptr;
            CURLcode res = curl_easy_getinfo(curl, CURLINFO_CAPATH, &caPath);
            if ((res == CURLE_OK) && caPath)
               logInfo("Using default directory for CA certificates: %s", caPath);

            curl_easy_cleanup(curl);
        }
    }
#endif
}

/*
    Sets the timeout for requests to \a timeoutSecs. A value of \c 0
    means the requests will never time out.
*/
void HttpClient::setRequestTimeout(long timeoutSecs)
{
    m_requestTimeout = timeoutSecs;
}

/*
    Sets the SSL certificate revocation behavior based on the value of \c option.
    This has only effect on Windows.
*/
void HttpClient::setCertRevokeBehavior(HttpRequest::CertRevokeOption option)
{
#ifdef _WIN32
    m_certRevokeBehavior = option;
#endif
}

} // namespace QLicenseService
