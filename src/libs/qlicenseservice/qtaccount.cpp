/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "qtaccount.h"

#include "commonsetup.h"
#include "httpclient.h"
#include "httpsettings.h"
#include "version.h"

#include <constants.h>
#include <licdsetup.h>
#include <qtaccountsetup.h>
#include <jsonhandler.h>
#include <utils.h>

using namespace QLicenseCore;

namespace QLicenseService {

static std::string getHostType()
{
    std::string hostType;
#if _WIN32
    hostType = "windows";
#elif __APPLE__ || __MACH__
    hostType = "mac";
#elif __linux__
    hostType = "linux";
#else
    hostTYpe = "unknown";
#endif
    return hostType;
}

static void setCommonRequestParameters(JsonHandler &payload, LicdSetup &settings)
{
    payload.set("service_version", DAEMON_VERSION);
    payload.set("host_type", getHostType());
    payload.set("host_word_size", std::to_string(utils::getHostWordSize()));
    payload.set("host_os", utils::getOsName());
    payload.set("hw_id", settings.get(sc_hwId));
    payload.set("hw_name", utils::getHostname());
    payload.set("src", LICD_SERVICE_DESCRIPTION);
    payload.set("src_version", DAEMON_VERSION);
}


bool QtAccount::performLogin(const std::string &email, const std::string &password)
{
    if (email.empty() || password.empty()) {
        m_error = "Cannot login with empty email or password";
        return false;
    }

    // TODO: no need to have our own settings instance once
    // daemon is the one to perform the login
    LicdSetup settings(LicdSetup::getQtAppDataLocation()
            + std::string(USER_SETTINGS_FOLDER_NAME) + DIR_SEPARATOR
            + std::string(DAEMON_SETTINGS_FILE));
    if (!settings.init()) {
        m_error = "Not able to read settings file required for login";
        return false;
    }

    HttpSettings httpSettings(&settings);
    const std::string accessPoint = settings.get(sc_loginAccessPoint);

    JsonHandler payload;
    setCommonRequestParameters(payload, settings);

    payload.set("email", email);
    payload.set("password", password);

    HttpClient http(httpSettings.serverAddress);
    http.setForceHttps(true);
    http.setCABundlePath(httpSettings.caBundlePath);
    http.setCertRevokeBehavior(httpSettings.certRevokeBehavior);
    http.setRequestTimeout(httpSettings.requestTimeout);

    HttpResponse reply;
    if (!http.sendReceive(reply, payload.dump(4), accessPoint, httpSettings.serverAddress)) {
        m_error = "Error while performing request";
        return false;
    }

    JsonHandler replyJson(reply.body);
    if (!replyJson.get("jwt", m_jwt)) {
        m_error = "Unexpected reply";
        std::string message;
        if (replyJson.get("message", message))
            m_error = message;

        return false;
    }

    m_email = email;
    m_loggedIn = true;
    return true;
}

bool QtAccount::writeToDisk()
{
    const std::string settingsFilePath = LicdSetup::getQtAppDataLocation() + QT_ACCOUNT_SETTINGS_FILE;
    logDebug("Writing Qt Account settings to disk: %s", settingsFilePath.c_str());

    if (!QtAccountSetup::createSettingsFile(settingsFilePath, m_email, m_jwt)) {
        m_error = "Could not write Qt Account settings file";
        return false;
    }

    return true;
}

} // namespace QLicenseService
