/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "tcpserver.h"

#include "commonsetup.h"

#include <pipe.h>
#include <utils.h>
#include <logger.h>

#include <algorithm>

using namespace QLicenseCore;

namespace QLicenseService {

TcpServer::TcpServer()
    : QLicenseCore::TcpMessageProtocol(TcpMessageProtocol::Capabilities::ResponseLengthHeader)
    , m_stopRequested(false)
    , m_running(false)
{
}

TcpServer::~TcpServer()
{
    stop();

    doCloseSocket(m_masterSocket);
    for (auto socket : m_clientSockets)
        doCloseSocket(socket.second.fd);
}

void TcpServer::start()
{
    if (m_running)
        return; // do not allow starting twice

    m_thread = std::thread(&TcpServer::run, this);
    m_stopRequested.store(false);
    m_running = true;
}

void TcpServer::stop()
{
    if (!m_running)
        return;

    m_stopRequested.store(true);

    std::unique_lock<std::mutex> lock(m_mutex);
    m_condvar.wait(lock, [&]{ return !m_running; });

    if (m_thread.joinable())
        m_thread.join();
}

bool TcpServer::init(uint16_t &serverPort)
{
#ifdef _WIN32
    // Initialize Winsock
    WSADATA wsaData;
    int iResult = 0;
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != NO_ERROR) {
        logError("Failed to initialize Winsock, WSAStartup() failed. "
                 "Unable to create a socket for communicating with client applications.");
        return false;
    }
#endif
    if (!createListeningSocket(m_masterSocket, serverPort)) {
        logError("Unable to create a socket for communicating with client applications.");
        return false;
     }

    logInfo("Waiting for connections ...");

    return true;
}

void TcpServer::run()
{
    utils::setBlockSignalsMask();

    while (!m_stopRequested.load()) {
        m_readfds.clear();
#ifdef _WIN32
        m_readfds.push_back({ m_masterSocket, POLLRDNORM });
        for (auto &socket : m_clientSockets)
            m_readfds.push_back({ socket.second.fd, POLLRDNORM });

        // Wait for an activity on one of the sockets.
        int activity = WSAPoll(m_readfds.data(), m_readfds.size(), 1000);
#else
        m_readfds.push_back({ m_masterSocket, POLLIN, 0 });
        for (auto &socket : m_clientSockets)
            m_readfds.push_back({ socket.second.fd, POLLIN, 0 });

        // Wait for an activity on one of the sockets.
        int activity = poll(m_readfds.data(), m_readfds.size(), 1000);
#endif
        if ((activity < 0) && (errno != EINTR)) {
            logError("Socket activity monitoring failed: %s", getLastSocketError().c_str());
            continue;
        } else if (activity == 0) {
            // timeout
            continue;
        }

        for (auto &pollfd : m_readfds) {
            if (pollfd.revents == 0)
                continue; // no events for this socket

            // If anything happens on the master socket, its an incoming connection
            if ((pollfd.fd == m_masterSocket) && (pollfd.revents & POLLIN)) {
                int addrlen = sizeof(m_address);
                type_socket new_socket;
                if ((new_socket = accept(m_masterSocket,
                        (struct sockaddr *)&m_address, (socklen_t *)&addrlen)) < 0) {
                    logError("Socket accept(): Connection failed: %s", getLastSocketError().c_str());
                    continue;
                }
#ifdef _WIN32
                if (new_socket != INVALID_SOCKET) {
#else
                if (new_socket > 0) {
#endif
                    // Add new socket to set of sockets
                    uint16_t socketId = utils::generateUniqueShortId();
                    m_clientSockets.insert(std::make_pair<uint16_t &, ClientSocket>(socketId, ClientSocket{new_socket}));
                    logDebug("New connection - added to list of sockets with id %d", socketId);
                }
                continue;
            }

            auto socket = std::find_if(m_clientSockets.begin(), m_clientSockets.end(),
               [pollfd](const decltype(m_clientSockets)::value_type &pair) {
                   return pair.second.fd == pollfd.fd;
               });

            if (socket == m_clientSockets.end()) {
                logError("Could not find a socket that got an event!");
                continue;
            }

            if (pollfd.revents & POLLERR) {
                int error = 0;
                socklen_t len = sizeof(error);
#if _WIN32
                const int optResult = getsockopt(socket->second.fd, SOL_SOCKET, SO_ERROR, (char *)&error, &len);
#else
                const int optResult = getsockopt(socket->second.fd, SOL_SOCKET, SO_ERROR, &error, &len);
#endif
                if ((optResult == 0) && (error != 0))
                    logError("Error occurred with socket id %d: %s", socket->first, strerror(error));
                else
                    logDebug("Socket id %d became unusable. The client may have performed a hard shutdown sequence.", socket->first);

                closeClientSocket(socket->first);
                continue;
            }

            if (pollfd.revents & POLLNVAL) {
                // Invalid socket was used, close descriptor
                logError("Invalid socket descriptor %d was used, id: %d", socket->second, socket->first);
                closeClientSocket(socket->first);
                continue;
            }

            if (pollfd.revents & POLLHUP) {
                // Hang-up detected, client disconnected
                logDebug("Connection hang up for socket id: %d.", socket->first);
                closeClientSocket(socket->first);
                continue;
            }

            if (pollfd.revents & POLLIN) { //
                // It's IO operation on client socket. Check if it was for closing,
                // otherwise read the incoming message
                uint32_t capabilities = 0;
                if (readPeerCapabilities(socket->second.fd, capabilities))
                    socket->second.capabilities = capabilities;
#ifdef _WIN32
                int bytesRead = recv(socket->second.fd, m_buffer, sizeof(m_buffer), 0);
#else
                int bytesRead = read(socket->second.fd, m_buffer, sizeof(m_buffer));
#endif
                if (bytesRead == -1) {
#ifdef _WIN32
                    // Remote side executed hard close
                    if (WSAGetLastError() == WSAECONNRESET)
                        closeClientSocket(socket->first);
#endif
                } else if (bytesRead == 0) {
                    // Graceful shutdown, remove the socket from the monitored set and close it
                    closeClientSocket(socket->first);
                } else if (bytesRead > 0) {
                    std::lock_guard<std::mutex> lock(m_mutex);

                    TcpMessage msg;
                    msg.socketId = socket->first;
                    msg.message = std::string(m_buffer, bytesRead);
                    m_messages.push_back(msg);

                    Pipe::globalObject()->write(PIPE_MSG_READY_READ,
                        Pipe::Type::TcpServer, std::to_string(msg.socketId));
                }
            }
        }
    }
    std::lock_guard<std::mutex> lock(m_mutex);
    m_running = false;
    m_condvar.notify_all();
}

bool TcpServer::dataAvailable()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return !m_messages.empty();
}

std::string TcpServer::receive(uint16_t &socketId)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    if (m_messages.empty()) {
        // We never should end up here, but check anyway
        logError("No messages available in receive()");
        return "";
    }
    std::string message = m_messages.front().message;
    socketId = m_messages.front().socketId;
    m_messages.pop_front();
    return message;
}

ClientSocket TcpServer::clientSocket(uint16_t socketId)
{
    auto it = m_clientSockets.find(socketId);
    if (it == m_clientSockets.end()) {
        logWarn("Cannot find socket id %d", socketId);
        return ClientSocket();
    }

    return it->second;
}

/*
    Sends \a message response to \c socketId. Returns \c 0 on success, non-zero otherwise.
    If the client is capable of receiving a message length header, the length header is sent
    before the actual message payload.
*/
int TcpServer::sendResponse(uint16_t socketId, const std::string &message)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    auto it = m_clientSockets.find(socketId);
    if (it == m_clientSockets.end()) {
        logWarn("Skip response: socket ID %d not open.", socketId);
        return 1;
    }

    std::string payload = createResponsePayload(it->second.capabilities, message);
    if (payload.empty() || payload.length() > MAX_TCP_MESSAGE_LENGTH) {
        logError("The message size \"%d\" for socket ID %d is out of bounds", payload.length(), socketId);
        return 1;
    }

    type_socket socketFd = it->second.fd;
    if (send(socketFd, payload.c_str(), payload.length(), 0) != payload.length()) {
        logError("Send failed, socket ID %d: %s", socketId, getLastSocketError().c_str());
        return 1;
    }
    return 0;
}

void TcpServer::closeClientSocket(uint16_t socketId)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    auto it = m_clientSockets.find(socketId);
    if (it == m_clientSockets.end()) {
        logWarn("Cannot close socket id %d: not open.", socketId);
        return;
    }

    // Someone disconnected: Inform upper level
    TcpMessage msg;
    msg.socketId = socketId;
    msg.message = SOCKET_CLOSED_MSG;
    m_messages.push_back(msg);
    Pipe::globalObject()->write(PIPE_MSG_READY_READ, Pipe::Type::TcpServer, std::to_string(msg.socketId));

    logDebug("Closing socket %d", socketId);
    type_socket socketFd = m_clientSockets.at(socketId).fd;
    m_clientSockets.erase(socketId);

    doCloseSocket(socketFd);
}

void TcpServer::doCloseSocket(type_socket &socketFD)
{
#if __APPLE__ || __MACH__  || __linux__
    close(socketFD);
#else
    // causes also implicit shutdown sequence
    closesocket(socketFD);
#endif
}

bool TcpServer::createListeningSocket(type_socket &listeningSocket, uint16_t &port)
{
    // Create a master socket
    if ((listeningSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == 0) {
        logError("Socket creation failed %s", getLastSocketError().c_str());
        return false;
    }

    // Set master socket to allow multiple connections
    int opt = TRUE;
#ifdef _WIN32
    if (setsockopt(listeningSocket, SOL_SOCKET, SO_EXCLUSIVEADDRUSE, (char *)&opt, sizeof(opt)) < 0) {
#else
    if (setsockopt(listeningSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0) {
#endif
        logError("setsockopt failed: %s", getLastSocketError().c_str());
        return false;
    }
    timeval tv {0, 0}; // t.tv_sec = 0; t.tv_usec = 0;
    int ret = setsockopt(listeningSocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(timeval));
    if (ret < 0) {
        logError("Socket timeout: %s", getLastSocketError().c_str());
        return false;
    }

    // Type of socket created
    m_address.sin_family = AF_INET;
    m_address.sin_addr.s_addr = INADDR_ANY;
    m_address.sin_port = htons(port);

    // Bind the socket to localhost
    if (bind(listeningSocket, (struct sockaddr *)&m_address, sizeof(m_address)) < 0) {
        logError("Socket bind failed: %s", getLastSocketError().c_str());
        doCloseSocket(listeningSocket);
        return false;
    }

    // Update socket address (port might have changed if automatically assigned to free port)
    socklen_t addr_len = sizeof(m_address);
    if (getsockname(listeningSocket, (struct sockaddr *)&m_address, &addr_len) < 0) {
        logError("Failed to get socket address: %s", getLastSocketError().c_str());
        doCloseSocket(listeningSocket);
        return false;
    }

    port = ntohs(m_address.sin_port);

    logInfo("Listening in port %d", port);
    // Specify a maximum of 4096 pending connections for the socket. This is only
    // a suggestion, the actual value can be lower based on system configuration.
    // - Since Linux 5.4, the default in /proc/sys/net/core/somaxconn file is 4096;
    //   in earlier kernels, the default value is 128
    // - On macOS this is currently limited (silently) to 128.
    // - On Windows Sockets 2, SOMAXCONN defaults to a large value (typically several hundred or more).
    if (listen(listeningSocket, MAX_CONN) < 0) {
        logError("Socket listen() failed: %s", getLastSocketError().c_str());
        doCloseSocket(listeningSocket);
        return false;
    }
    return true;
}

std::string TcpServer::getLastSocketError() const
{
#if _WIN32
    int errorCode = WSAGetLastError();
    LPWSTR buffer = nullptr;

    FormatMessageW(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, (DWORD)errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&buffer, 0, NULL
    );

    std::string errorString(utils::narrow(buffer));
    LocalFree(buffer);

    if (errorString.empty())
        errorString = "Unknown error";

    return errorString;
#else
    return std::string(strerror(errno));
#endif
}

} // namespace QLicenseService
