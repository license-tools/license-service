/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
 */

#include "licenseimport.h"

#include <commonsetup.h>
#include <logger.h>

#include <vector>

using namespace QLicenseCore;

namespace QLicenseService {

/*
    Constructs local license importer for local \a localUser
*/
LicenseImport::LicenseImport(LicenseCache &cache, const std::string &localUser)
    : m_licenseCache(cache), m_localUser(localUser)
{
}

/*
    Search and import license files from the given \a searchPaths. Any previously
    imported licenses that do not exist anymore in the \a searchPaths are removed
    from the license cache.

    Licenses that are in use by client applications are not modified.
*/
void LicenseImport::sync(const std::vector<std::string> &searchPaths)
{
    std::vector<License *> insertedLicenses;

    // 1. Insert new licenses to cache and update existing ones
    std::vector<std::string> allMatches;
    for (const auto &searchPath : searchPaths) {
        auto ret = utils::getDirListing(searchPath, LICENSE_FILE_PREFIX);
        for (const std::string &fileName : ret) {
            const std::string fullPath = searchPath + DIR_SEPARATOR + fileName;
            // Don't override licenses that are already in use
            if (licenseInUse(fullPath)) {
                logDebug("Ignoring license file currently in use: %s", fullPath.c_str());
                continue;
            }

            if (License *license = m_licenseCache.insertLocalLicense(fullPath, m_localUser)) {
                logDebug("Successfully imported local license file: %s", fullPath.c_str());
                insertedLicenses.push_back(license);
            } else {
                logWarn("%s: %s", fullPath.c_str(), m_licenseCache.error().c_str());
            }
        }
    }

    // 2. Remove cached licenses for deleted license files
    const std::vector<License *> localOnlyLicenses = localLicenses();
    int removedCount = 0;
    for (auto *license : localOnlyLicenses) {
        auto it = std::find(insertedLicenses.begin(), insertedLicenses.end(), license);
        if (it == insertedLicenses.end()) {
            // Don't remove licenses that are still in use
            if (licenseInUse(license)) {
                logDebug("Ignoring license id currently in use: %s", license->id().c_str());
                continue;
            }

            const std::string licenseId = license->id();
            if (m_licenseCache.remove(m_localUser, licenseId)) {
                logDebug("Removed imported license id %s due to deleted source license file", licenseId.c_str());
                ++removedCount;
            } else {
                logWarn("License id %s: %s", licenseId.c_str(), m_licenseCache.error().c_str());
            }
        }
    }

    if (removedCount > 0)
        logInfo("Removed %d obsolete imported license(s)", removedCount);
}

std::vector<License *> LicenseImport::localLicenses() const
{
    const std::vector<License *> allLicenses = m_licenseCache.licenses(m_localUser);
    std::vector<License *> localLicenses;

    for (auto *license : allLicenses) {
        for (auto *reservation : license->reservations()) {
            // Check if this license has a local-only reservation (imported license)
            if (reservation->isLocalOnly()) {
                localLicenses.push_back(license);
                break;
            }
        }
    }

    return localLicenses;
}

bool LicenseImport::licenseInUse(const std::string &licenseFile) const
{
    std::string jsonData;
    if (!utils::readFile(jsonData, licenseFile))
        return false;

    std::unique_ptr<License> license(new License(m_localUser));
    Status result = license->parseFromJson(jsonData);
    if (result != Status::SUCCESS)
        return false;

    if (License *existingLicense = m_licenseCache.license(m_localUser, license->id()))
        return licenseInUse(existingLicense);

    return false;
}

bool LicenseImport::licenseInUse(License *license) const
{
    for (auto *reservation : license->reservations()) {
        if (reservation->clients().size() > 0)
            return true;
    }
    return false;
}

} // namespace QLicenseService
