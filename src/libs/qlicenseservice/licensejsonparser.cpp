/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licensejsonparser.h"

#include "commonsetup.h"
#include "license.h"
#include "request.h"

#include <errors.h>
#include <utils.h>

using namespace QLicenseCore;

namespace QLicenseService {

// Edit always when the server support for these is changing
static const std::unordered_map<std::string, License::Model> sc_knownLicenseModels = {
    {"site",        License::Model::Site},
    {"floating",    License::Model::Float},
    {"named",       License::Model::Named},
    {"unknown",     License::Model::None}
};

static const std::unordered_map<std::string, License::ReservationType> sc_knownReservationTypes = {
    {"process",     License::ReservationType::Process},
    {"user",        License::ReservationType::User}
};

static const std::unordered_map<std::string, License::RenewalPolicy> sc_knownRenewalPolicies = {
    {"active",     License::RenewalPolicy::Active},
    {"passive",    License::RenewalPolicy::Passive}
};


Status LicenseJsonParser::parse(const std::string &data, License &license)
{
    m_json.reset(new JsonHandler(data));
    m_error.clear();

    const std::string version = preParseVersion();
    if (version.empty()) {
        m_error = "Could not parse version from license JSON";
        return Status::INVALID_RESPONSE;
    }

    try {
        ParseLicenseFunction parseLicense = findParser(version);
        return parseLicense(license);
    } catch (const QLicenseCore::Error &e) {
        m_error = e.what();
        return Status::INCOMPATIBLE_LICENSE_FORMAT_VERSION;
    }
}

Status LicenseJsonParser::parseResponse(const std::string &data, const RequestInfo *request,
    const License &license, std::string &response)
{
    m_json.reset(new JsonHandler(data));
    m_error.clear();

    const std::string version = preParseVersion();
    if (version.empty()) {
        m_error = "Could not parse version from license JSON";
        return Status::INVALID_RESPONSE;
    }

    try {
        ParseLicenseResponseFunction parseLicenseResponse = findResponseParser(version);
        return parseLicenseResponse(request, license, response);
    } catch (const QLicenseCore::Error &e) {
        m_error = e.what();
        return Status::INCOMPATIBLE_LICENSE_FORMAT_VERSION;
    }
}

std::string LicenseJsonParser::error() const
{
    return m_error;
}

std::string LicenseJsonParser::preParseVersion() const
{
    std::string version;
    m_json->get("license.version", version);
    return version;
}

/*
    Tries to find the least major-minor version parser supporting the license format
    and returns the parser method. Throws QLicenseService::Error on failure.
*/
ParseLicenseFunction LicenseJsonParser::findParser(const std::string &version) const
{
    for (auto &parser : m_serverResponseParsers) {
        if (utils::compareVersion(version, parser.first, 2) <= 0)
            return parser.second;
    }

    throw QLicenseCore::Error("No suitable parser found for license version: " + version);
}

ParseLicenseResponseFunction LicenseJsonParser::findResponseParser(const std::string &version) const
{
    for (auto &parser : m_clientResponseParsers) {
        if (utils::compareVersion(version, parser.first, 2) <= 0)
            return parser.second;
    }

    throw QLicenseCore::Error("No suitable parser found for license version: " + version);
}

Status LicenseJsonParser::parseLicenseV10(License &license)
{
    // 1. Parse mandatory license information
    std::string model;
    m_json->get("license.license_info.model", model);
    if (sc_knownLicenseModels.find(model) == sc_knownLicenseModels.end())
        return Status::UNKNOWN_LICENSE_MODEL;

    license.m_model = sc_knownLicenseModels.at(model);

    std::string licenseType;
    m_json->get("license.license_info.type", licenseType);
    if (licenseType == "perpetual")
        license.m_perpetual = true;

    std::string reservationType;
    m_json->get("license.pool_info.reservation_type", reservationType);
    if (sc_knownReservationTypes.find(reservationType) == sc_knownReservationTypes.end())
        return Status::UNKNOWN_RESERVATION_TYPE;

    license.m_reservationType = sc_knownReservationTypes.at(reservationType);

    if (!m_json->get("license.license_info.license_id", license.m_id)) {
        m_error = "No license id found";
        return Status::INVALID_RESPONSE;
    }

    if (!m_json->get("license.license_info.priority", license.m_priority)) {
        m_error = "No license priority found";
        return Status::INVALID_RESPONSE;
    }

    // 2. Parse license consumers
    JsonData::array_t consumers;
    m_json->get("license.consumers", consumers);

    try {
        for (const auto& obj : consumers) {
            License::Consumer consumer;
            consumer.data = obj.dump();
            consumer.id = obj["consumer_id"];
            consumer.version = obj["consumer_version"];

            license.m_consumers.insert(consumer);
        }
    } catch (const nlohmann::json::exception& e) {
        m_error = "Cannot parse element from JSON: " + std::string(e.what());
        return Status::INVALID_RESPONSE;
    }

    // 3. Parse signature information
    if (!m_json->get("cs_p", license.m_cs_p)) {
        m_error = "No public key found for license";
        return Status::INVALID_RESPONSE;
    } else if (!m_json->get("cs_s", license.m_cs_s)) {
        m_error = "No signature found for license";
        return Status::INVALID_RESPONSE;
    }

    // 5. Dig out lease related properties
    std::string renewalPolicy;
    m_json->get("license.pool_info.renewal_policy", renewalPolicy);
    if (!renewalPolicy.empty()) {
        if (sc_knownRenewalPolicies.find(renewalPolicy) != sc_knownRenewalPolicies.end())
            license.m_renewalPolicy = sc_knownRenewalPolicies.at(renewalPolicy);
        else
            logDebug("Unknown renewal policy type '%s'. Defaulting to active renewal.", renewalPolicy.c_str());
    }

    std::string leaseUnit;
    if (!m_json->get("license.pool_info.lease_time", license.m_leaseTimeSecs)) {
        m_error = "No license.pool_info.lease_time found for license: " + license.m_id;
        return Status::INVALID_RESPONSE;
    }
    if (!m_json->get("license.pool_info.lease_time_unit", leaseUnit)) {
        m_error = "No license.pool_info.lease_time_unit found for license: " + license.m_id;
        return Status::INVALID_RESPONSE;
    }
    if (leaseUnit == "hour")
        license.m_leaseTimeSecs = license.m_leaseTimeSecs * SECS_IN_HOUR;
    else if (leaseUnit == "day")
        license.m_leaseTimeSecs = license.m_leaseTimeSecs * SECS_IN_DAY;
    // else seconds - no need to convert

    if (license.m_leaseTimeSecs <= 0)
        return Status::INVALID_RESPONSE; // Something is off with the lease time

    std::string licenseValidTo;
    if (!m_json->get("license.license_info.valid_to", licenseValidTo)) {
        m_error = "No license.pool_info.valid_to found for license: " + license.m_id;
        return Status::INVALID_RESPONSE;
    }
    license.m_licenseValidToDate = utils::stringToUtcTime(licenseValidTo.c_str(), "%Y-%m-%d");

    std::string licenseValidFrom;
    if (!m_json->get("license.license_info.valid_from", licenseValidFrom)) {
        m_error = "No license.pool_info.valid_from found for license: " + license.m_id;
        return Status::INVALID_RESPONSE;
    }
    license.m_licenseValidFromDate = utils::stringToUtcTime(licenseValidFrom.c_str(), "%Y-%m-%d");

    return Status::SUCCESS;
}

/*
    Parses the the given license data into a string suitable for response to client library
*/
Status LicenseJsonParser::parseLicenseResponseV10(const RequestInfo *request, const License &license,
    std::string &response)
{
    try {
        nlohmann::json responseJson = *m_json->data();
        // 1. Replace consumers array with single object for this client
        // TODO: we may need to include the consumer array as well for completeness
        responseJson["license"].erase("consumers");
        const License::Consumer *consumer = license.consumer(request->consumerInfo.id, request->consumerInfo.version);
        if (!consumer)
            throw Error("Could not find a consumer from license object");

        responseJson["license"]["consumer"] = *const_cast<License::Consumer *>(consumer)->data.data();

        response = responseJson.dump(-1, ' ', false, nlohmann::json::error_handler_t::replace);
    } catch (const nlohmann::json::exception& e) {
        m_error = "Error parsing license JSON: " + std::string(e.what());
        return Status::INVALID_RESPONSE;
    } catch (const QLicenseCore::Error& e) {
        m_error = "Error constructing reply to client: " + std::string(e.what());
        return Status::INVALID_RESPONSE;
    }

    return Status::SUCCESS;
}

} // namespace QLicenseService
