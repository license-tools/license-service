/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "consumerappinfo.h"

#include <constants.h>
#include <logger.h>

using namespace QLicenseCore;

namespace QLicenseService {

ConsumerAppInfo::ConsumerAppInfo(const nlohmann::json &jsonObject)
{
    try {
        id = jsonObject.at(sc_consumerId);
        version = jsonObject.at(sc_consumerVersion);
        processId = jsonObject.at(sc_consumerProcessId);
    } catch (const nlohmann::json::exception &e) {
        logError("Error parsing JSON: %s", e.what());
    } catch (const std::out_of_range &e) {
        logError("Error parsing JSON: key not found: %s", e.what());
    }

    if (jsonObject.contains(sc_consumerInstallationSchema))
        installationSchema = jsonObject.at(sc_consumerInstallationSchema);

    if (jsonObject.contains(sc_consumerSupportedFeatures))
        supportedFeatures = jsonObject.at(sc_consumerSupportedFeatures);

    if (jsonObject.contains(sc_consumerBuildTimestamp))
        buildTimestamp = jsonObject.at(sc_consumerBuildTimestamp);
}

ConsumerAppInfo::ConsumerAppInfo(const std::string &consumerId, const std::string &consumerVersion,
                                 const std::string &consumerProcessId)
    : id(consumerId)
    , version(consumerVersion)
    , processId(consumerProcessId)
{
}

nlohmann::json ConsumerAppInfo::toJson() const
{
    try {
        nlohmann::json object;
        object[sc_consumerId] = id;
        object[sc_consumerVersion] = version;
        object[sc_consumerProcessId] = processId;

        if (!installationSchema.empty())
            object[sc_consumerInstallationSchema] = installationSchema;

        if (!supportedFeatures.empty())
            object[sc_consumerSupportedFeatures] = supportedFeatures;

        if (!buildTimestamp.empty())
            object[sc_consumerBuildTimestamp] = buildTimestamp;

        return object;
    } catch (const nlohmann::json::exception &e) {
        logError("Error constucting JSON: %s", e.what());
        return nlohmann::json();
    }
}

bool ConsumerAppInfo::operator==(const ConsumerAppInfo &other) const
{
    // processId intentionally left out, as it's changes per process if not overridden by client
    return id == other.id
           && version == other.version
           && installationSchema == other.installationSchema
           && supportedFeatures == other.supportedFeatures
           && buildTimestamp == other.buildTimestamp;
}

bool ConsumerAppInfo::operator!=(const ConsumerAppInfo &other) const
{
    return !(*this == other);
}

} // namespace QLicenseService
