/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "licenseprecheck.h"

#include <httpclient.h>
#include <licdsetup.h>
#include <localqtaccount.h>
#include <licenseclient.h>

#include <nlohmann/json.hpp>

namespace QLicensePrecheck {

class LicensePrecheckPrivate
{
public:
    LicensePrecheckPrivate(const std::string &serverAddr);

    std::string error() const;

    bool listLicenses(std::vector<QLicenseClient::LicenseInfo> &licenses);
    bool precheck(std::vector<Component> &components);

private:
    std::string prepareListRequestPayload();
    std::string preparePrecheckRequestPayload(const std::vector<Component> &components);

    bool parseListRequestResponse(const std::string response, std::vector<QLicenseClient::LicenseInfo> &licenses);
    bool parsePrecheckRequestResponse(const std::string response, std::vector<Component> &components);
    bool isSuccessResponse(const QLicenseService::HttpResponse &response);

    std::string authToken() const;

    void setError(const std::string &error);

    nlohmann::json componentToJson(const Component &component) const;
    bool isValidProperties(const QLicenseClient::ClientProperties &properties) const;

private:
    std::unique_ptr<QLicenseCore::LicdSetup> m_settings;
    std::unique_ptr<QLicenseService::HttpClient> m_http;

    QLicenseCore::LocalQtAccount m_account;

    std::string m_username;
    std::string m_error;
};

} // namespace QLicensePrecheck
