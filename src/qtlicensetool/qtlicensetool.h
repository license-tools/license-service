/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <licenseclient.h>
#include <commonsetup.h>

#define APP_NAME "Qt licensing CLI"

void showVersion();
bool askStatus(QLicenseClient::LicenseClient::StatusOperation operation);
bool clearReservations();
void performLogin();
void errorAndExit(const std::string &reason = "");
bool showTermsAndConditions();
bool acceptTermsAndConditions();
bool getServiceSetting(const std::string &key, std::string &value);
bool setServiceSetting(const std::string &key, const std::string &value,
    const QLicenseClient::LicenseClient::SettingsType &settingsType, std::string &reply);
void helpAndExit();
