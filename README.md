// Copyright (C) 2024 The Qt Company Ltd.

# Qt License Service for Windows, Linux, and macOS

The Qt License Service (a daemon) is a gateway between the Qt License Server and applications
like Qt framework, Qt Creator, and Qt Design Studio.

The applications will request a license before they can run. The client applications send the
request to the Qt License Service, which decides if the request needs to be sent to the
Qt License Server or can a cached reservation be utilized.

The communication between client applications and the Qt License Service is using TCP.
Whereas the communication between the Qt License Service and Qt License Server is using HTTPS.

The Qt License Service has two operating modes:

  1. Install it as a system service
    - accessible for all users on the same machine
    - runs on the background

  2. On-demand launch
    - started automatically on the user space when needed


# Install the Qt License Service

## Manual installation

The Qt License Service can be built from sources or pre-built binaries can be installed
via the Qt Online Installer.

  - To build from sources, install, and run, see each system's respective INSTALL_xxx.txt file
    - [INSTALL_unix.txt](INSTALL_unix.txt)
    - [INSTALL_win.txt](INSTALL_win.txt)
  - To install the pre-built binaries use the Qt Online Installer which can be downloaded from your
    [Qt Account](https://account.qt.io/)
    - In the installer UI locate the "License Management Tools" section

Once the Qt License Service binaries are installed on your system you need to define the server
address. See the [INSTALL_unix.txt](INSTALL_unix.txt) or [INSTALL_win.txt](INSTALL_win.txt) and
go to section [1].


### Enable license check in Qt Framework with moc_wrapper(.exe) (Qt5 and Qt6)

See the [INSTALL_unix.txt](INSTALL_unix.txt) or [INSTALL_win.txt](INSTALL_win.txt) and go to
section [4].

If the setup is made correctly (moc binary changed to the link pointing to mocwrapper), the
license checks should happen in the background without any user actions, each time a build
is being done.

You can ask mocwrapper version with:
```bash
  ./mocwrapper(.exe) --version
```

## Automatic installation via Qt Installer

The Qt License Service will be installed automatically by the installer for the products that
contain native integration to Qt License Service. Please check the roll-out timeline from
Qt Support.

The service will be installer under the QtSDK and registered for the on-demand launch.
There is no need for the user to configure anything.

Note! If some product(s) don't contain the native integration yet, then the manual step is
required for the given product.


# Authentication

## Cloud usage

Qt Account login needs to be active in order to make license reservations using the Qt hosted
license server. If you have used the Qt (Online) installer recently then the JWT token is likely
valid and no further actions are needed.

Also, some products, like Qt Creator or Qt Design Studio, contain Qt Account login dialog if the
login has expired.

A command line tool is provided to perform the login if needed:

```bash
  ./qtlicensetool(.exe) --login
```

Follow the instructions given by the tool.


## On-prem usage

Current logged username is read from the operating system and used as the authentication.
If needed, the current username can be overridden by:

Linux/macOS:
```bash
  export QTLICD_CLIENT_USERNAME=john
```

Windows:
```bash
  set QTLICD_CLIENT_USERNAME=john
```


# Configuration file summary

Service license cache:

  - Linux:       /home/<user>/.local/share/Qt/qtlicd/cache
  - macOS:       $HOME/Library/Application Support/Qt/qtlicd/cache
  - Windows:     C:\Users\<user>\AppData\Roaming\Qt\qtlicd\cache

Service configuration file:

  - Linux:       /home/<user>/.local/share/Qt/qtlicd/qtlicd.ini
  - macOS:       $HOME/Library/Application Support/Qt/qtlicd/qtlicd.ini
  - Windows:     C:\Users\<user>\AppData\Roaming\Qt\qtlicd\qtlicd.ini

On-demand startup registration file:

  - Linux:       /home/<user>/.local/share/Qt/qtlicd/installations.ini
  - macOS:       $HOME/Library/Application Support/Qt/qtlicd/installations.ini
  - Windows:     C:\Users\<user>\AppData\Roaming\Qt\qtlicd\installations.ini

On-demand TCP port file:

  - Linux:       /home/<user>/.local/share/Qt/qtlicd/port.ini
  - macOS:       $HOME/Library/Application Support/Qt/qtlicd/port.ini
  - Windows:     C:\Users\<user>\AppData\Roaming\Qt\qtlicd\port.ini

Qt Account configuration file:

  - Linux        /home/<user>/.local/share/Qt/qtaccount.ini
  - macOS:       $HOME/Library/Application Support/Qt/qtaccount.ini
  - Windows:     C:\Users\<user>\AppData\Roaming\Qt\qtaccount.ini


# Diagnosis

## Test the connection to the Qt License Server

You can ping the server and ask its version to verify that the connection is working.
```bash
  qtlicensetool(.exe) --serverversion
```

## qtlicensetool(.exe)
The qtlicensetool(.exe) is a tool to show active reservations, and to
get version information.

Show qtlicensetool(.exe) version
```bash
  qtlicensetool(.exe) --version
```

Show Qt License Service version
```bash
  qtlicensetool(.exe) --daemonversion
```

Show Qt License Server version
```bash
  qtlicensetool(.exe) --serverversion
```

Print out the list of active reservations
```bash
  qtlicensetool(.exe) --reservations
```


# Qt License Service logging feature

Normally, as a system service, the Qt License Service keeps a log of its operation in its
working directory:

  - Linux/macOS: /opt/qtlicd/qtlicd_log.txt
  - Windows: C:\Program Files\qtlicd\qtlicd_log.txt

When running as a system service, the Qt License Service prints only in its logfile and nothing
to stdout. The default log level is "info".

If the Qt License Service is invoked from the command line, i.e. running the service in
user space, the logging feature can be controlled by the following options:

```bash
  --log-level <none|error|warning|info|debug> : Set the logging level
  --nologfile                                 : Disable logging in the file
  --nostdout                                  : Disable logging in stdout
```

By default, the "--mode cli" option causes both logfile and stdout prints to be enabled
and saves the log file in the current working directory (i.e. where it was invoked from).

When running in user space (on-demand mode), the log file is written to the directory
from which the service executable was started.

Note that log levels are set so that logs are shown up to the wanted level. So if you set the log
level to be "error", only error messages are shown. If you set it to "info", all except debug
messages are shown.

Example:
```bash
    qtlicd(.exe) --mode cli --log-level error --nologfile
     - With this, the Qt License Service starts in console mode, showing only errors in the
       console, but writes nothing to the log file.
```

When running as a system service, the logging feature can be controlled as follows:

  - Linux: Add QTLICD_LOG_LEVEL environment variable from the command line and restart the service:
```bash
     sudo systemctl set-environment QTLICD_LOG_LEVEL=<desired log level here>
     sudo systemctl restart qtlicd
```

  macOS: Edit the .plist file as root
```bash
      sudo vim /Library/LaunchDaemons/oi.qt.qtlicd.plist  <-- replace 'vim' with the editor you
                                                              prefer
```
    - Find the items:
        <string>--log-level</string>
        <string>info</string>
    - Change default string "info" to the desired log level
         (make sure not to break the file's .xml format)
    - Restart service:
```bash
      sudo launchctl unload -w /Library/LaunchDaemons/io.qt.qtlicd
      sudo launchctl load -w /Library/LaunchDaemons/io.qt.qtlicd
```

  - Windows: Add the QTLICD_LOG_LEVEL environment variable with the desired value to the system
             environment variables, and restart the service.

Note! Make sure to keep track of the log file size, then delete or copy it elsewhere whenever it
seems to grow too big.


# FAQ

1. Q: I killed the qtlicd process manually and now it appears to to misfunction?

   A: The service attempts to recover if it was shut down forcefully, but you can try these
      steps to resolve the issue:
      - Delete the license cache folder:
        Linux:       /home/<user>/.local/share/Qt/qtlicd/cache
        macOS:       $HOME/Library/Application Support/Qt/qtlicd/cache
        Windows:     C:\Users\<user>\AppData\Roaming\Qt\qtlicd\cache

        Note! This will delete your current local reservations if such exist!

2. Q: The On-demand start of the service does not work?

   A: There can be the following reasons for this:

     - The service is not registered correctly. Open the following file:
        Linux:       /home/<user>/.local/share/Qt/qtlicd/installations.ini
        macOS:       $HOME/Library/Application Support/Qt/qtlicd/installations.ini
        Windows:     C:\Users\<user>\AppData\Roaming\Qt\qtlicd\installations.ini

        It should contain one or multiple sections, like:

        [_home_user_some_path_here_qtlicd]
        path=/home/<user>/<some_path_here>/qtlicd
        source=qt_installer
        timestamp=2024-02-14 08:21:09
        version=3.0.0

        If this is not present then try to manually register the service:

        $ <service installation path>/qtlicd(.exe) --register <optional argument for the source>

        Check the 'installations.ini' again.

     - The On-demand automatic TCP port configuration file cleanup has failed. Delete the file:

        Linux:       /home/<user>/.local/share/Qt/qtlicd/port.ini
        macOS:       $HOME/Library/Application Support/Qt/qtlicd/port.ini
        Windows:     C:\Users\<user>\AppData\Roaming\Qt\qtlicd\port.ini

3. Q: I'm getting a "Problem with the SSL CA cert (path? access rights?)" error message from license service?

   A: This is likely due to invalid CA bundle path. By default the path is attempted to be resolved
      automatically on Linux. If this fails, an absolute path value can be set manually in the qtlicd.ini
      configuration file, for example on Ubuntu 22.04:

      ca_bundle_path=/etc/ssl/certs/ca-certificates.crt
