/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#include "testapp.h"

#include "constants.h"
#include "utils.h"
#include "jsonhandler.h"

#include <iostream>

static const char sc_tab[] = "    ";

TestApp::TestApp(const std::string &consumerId, const std::string &consumerVersion,
                 const std::string &consumerBuildTimestamp, bool extendedLicenseCheck)
    : m_consumerId(consumerId)
    , m_consumerVersion(consumerVersion)
    , m_consumerBuildTimestamp(consumerBuildTimestamp)
    , m_extendedLicenseCheck(extendedLicenseCheck)
    , m_client(new LicenseClient())
    , m_precheck(new LicensePrecheck())
{

}

TestApp::~TestApp()
{}

bool TestApp::requestLicense()
{
    std::unique_lock<std::mutex> lock(m_mutex);

    std::string error;
    if (!m_client->init(error, 30)) {
        std::cout << "Error while initializing license client: " << error << std::endl;
        return false;
    }

    if (m_extendedLicenseCheck) {
        m_client->setClientSetting(QLicenseClient::ClientSettings::sc_extendedLicenseCheck,
            QLicenseClient::ClientSettings::sc_true);
    }

    bool success = false;
    // Test prints for callbacks
    auto onReservationStatusChanged = [&](LicenseReservationInfo *info) {
        if (info->status() == StatusCode::Success) {
            std::cout << "License reserved/renewed successfully:" << std::endl;
            printReservationInfo(*info);
            success = true;
        } else {
            std::cout << "License rejected/revoked:" << std::endl;
            std::cout << sc_tab << "Message: " << info->message() << std::endl;
            std::cout << sc_tab << "Status: " << static_cast<int>(info->status()) << std::endl;
        }
        m_cv.notify_all();
    };

    if (!m_client->reserve(getClientProperties(), onReservationStatusChanged))
        return false;

    m_cv.wait(lock);
    return success;
}

bool TestApp::listLicenses()
{
    std::vector<LicenseInfo> licenses;
    if (!m_precheck->listLicenses(licenses)) {
        std::cout << "Error while listing licenses: " << m_precheck->error() << std::endl;
        return false;
    }

    for (auto &license : licenses)
        printLicenseInfo(license);

    return true;
}

bool TestApp::precheckInstallation()
{
    Component component;
    component.componentId = "org.qtproject.example";
    component.clientProperties = getClientProperties();

    std::vector<Component> components;
    components.push_back(component);

    if (!m_precheck->precheck(components)) {
        std::cout << "Error while prechecking components: " << m_precheck->error() << std::endl;
        return false;
    }

    for (auto &component : components) {
        std::cout << sc_tab << "Component id: " << component.componentId << std::endl;
        std::cout << sc_tab << "Installation allowed: " << component.installationAllowed << std::endl;
        std::cout << sc_tab << "Consumer id: " << component.clientProperties.consumerId << std::endl;
        std::cout << sc_tab << "Consumer version: " << component.clientProperties.consumerVersion << std::endl;
        std::cout << std::endl;
    }

    return true;
}

ClientProperties TestApp::getClientProperties() const
{
    ClientProperties properties;
    // Fetching username and jwt is up to CIP
    properties.consumerId = m_consumerId.empty() ? TEST_APP_NAME : m_consumerId;
    properties.consumerVersion = m_consumerVersion.empty() ? TEST_APP_VERSION : m_consumerVersion;
    properties.consumerSupportedFeatures = {"qt", "ios"}; // example values for testing
    properties.consumerInstallationSchema = "commercial";
    properties.consumerBuildTimestamp = m_consumerBuildTimestamp;

    return properties;
}

void TestApp::printLicenseInfo(LicenseInfo &info)
{
    std::string licenseId;
    info.licenseInfo(LicenseInfoType::Id, licenseId);
    std::string expiryDate;
    info.licenseInfo(LicenseInfoType::LicenseValidTo, expiryDate);
    std::string model;
    info.licenseInfo(LicenseInfoType::Model, model);
    std::string schema;
    info.licenseInfo(LicenseInfoType::Schema, schema);

    std::cout << sc_tab << "License id: " << licenseId << std::endl;
    std::cout << sc_tab << "License expires at: " << expiryDate << std::endl;
    std::cout << sc_tab << "Licensee admin: " << info.contactsInfo().admin << std::endl;
    std::cout << sc_tab << "Licensee contact: " << info.licenseeInfo().contact << std::endl;
    std::cout << sc_tab << "License model: " << model << std::endl;
    std::cout << sc_tab << "License schema: " << schema << std::endl;
    for (auto &consumer : info.consumers()) {
        std::cout << sc_tab << "Consumer id: " << consumer.consumerId << std::endl;
        if (!consumer.consumerVersion.empty())
            std::cout << sc_tab << sc_tab << "Consumer version: " << consumer.consumerVersion << std::endl;
        std::cout << sc_tab << sc_tab << "Consumer priority: " << consumer.consumerPriority << std::endl;
        if (!consumer.consumerFeatures.empty()) {
            std::cout << sc_tab << sc_tab << "Consumer features: ";
            for (auto &feature: consumer.consumerFeatures)
                std::cout << feature.first << "=" << feature.second << ", ";
            std::cout << std::endl;
        }
    }
    std::cout << "####################" << std::endl;
}

void TestApp::printReservationInfo(LicenseReservationInfo &info)
{
    std::string expiryDate;
    info.licenseInfo(LicenseInfoType::LicenseValidTo, expiryDate);

    std::string licenseId;
    info.licenseInfo(LicenseInfoType::Id, licenseId);

    std::cout << sc_tab << "Message: " << info.message() << std::endl;
    std::cout << sc_tab << "License id: " << licenseId << std::endl;
    std::cout << sc_tab << "License expires at: " << expiryDate << std::endl;
    std::cout << sc_tab << "Licensee admin: " << info.contactsInfo().admin << std::endl;
    std::cout << sc_tab << "Licensee contact: " << info.licenseeInfo().contact << std::endl;

    std::map<std::string, std::string> features = info.licenseFeatures();
    std::cout << sc_tab << "Features:\n";
    for (const auto &feature : features)
        std::cout << sc_tab << "  " << feature.first << ": " << feature.second << std::endl;

    std::string leaseValidFrom;
    std::string leaseValidTo;
    info.licenseInfo(LicenseInfoType::LeaseValidFrom, leaseValidFrom);
    info.licenseInfo(LicenseInfoType::LeaseValidTo, leaseValidTo);

    std::cout << sc_tab << "Reservation granted at " << leaseValidFrom << std::endl;
    std::cout << sc_tab << "Reservation valid until " << leaseValidTo << std::endl;
}
