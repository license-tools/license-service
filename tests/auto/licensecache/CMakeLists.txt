# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

set(TEST_JSON1 ${CMAKE_CURRENT_LIST_DIR}/license_1.json)
set(TEST_JSON2 ${CMAKE_CURRENT_LIST_DIR}/license_2.json)
set(TEST_JSON3 ${CMAKE_CURRENT_LIST_DIR}/license_3.json)
set(TEST_JSON4 ${CMAKE_CURRENT_LIST_DIR}/license_4.json)
set(TEST_JSON5 ${CMAKE_CURRENT_LIST_DIR}/license_5.json)
set(TEST_JSON5 ${CMAKE_CURRENT_LIST_DIR}/license_5.json)
set(TEST_LICENSES_HOME_DIR ${CMAKE_CURRENT_LIST_DIR})
set(TEST_EXISTING_CACHE_DIR ${CMAKE_CURRENT_LIST_DIR}/cache)
configure_file(testdata.h.in testdata.h)

add_executable(tst_licensecache tst_licensecache.cpp)
target_link_libraries(tst_licensecache PRIVATE Catch2 qlicenseservice)

add_test(NAME tst_licensecache COMMAND tst_licensecache)

include_directories("${CMAKE_BINARY_DIR}/tests/auto/licensecache")
