/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "testdata.h"

#include <fstream>

#include <utils.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

using namespace QLicenseCore;

std::chrono::system_clock::time_point createTimePoint(int year, int month, int day,
    int hour, int minute, int second, int milliseconds = 0)
{
    std::tm timeInfo = {};
    timeInfo.tm_year = year - 1900; // tm_year is years since 1900
    timeInfo.tm_mon = month - 1;    // tm_mon is 0-based (0 = January)
    timeInfo.tm_mday = day;
    timeInfo.tm_hour = hour;
    timeInfo.tm_min = minute;
    timeInfo.tm_sec = second;

    std::time_t time = std::mktime(&timeInfo) - utils::getTimezoneOffset();
    if (time == -1)
        return std::chrono::system_clock::time_point{};

    auto timePoint = std::chrono::system_clock::from_time_t(time);
    timePoint += std::chrono::milliseconds(milliseconds);

    return timePoint;
}


TEST_CASE_METHOD(InitCleanupFixture, "Version compare", "[utils]")
{
    // {lhsVersion, rhsVersion, parts, return value}
    auto data = GENERATE(table<std::pair<std::string, std::string>, std::pair<int, int>>({
        // Full compare
        {std::make_tuple(std::make_pair("1.0.0", "1.0.0"), std::make_pair(0, 0))},
        {std::make_tuple(std::make_pair("1.0.0", "1.1.0"), std::make_pair(0, -1))},
        {std::make_tuple(std::make_pair("1.0.0", "2.0.1.0"), std::make_pair(0, -1))},
        {std::make_tuple(std::make_pair("1.0.0", "1.0.0-1"), std::make_pair(0, -1))},
        {std::make_tuple(std::make_pair("1.0.0", "0.2.1"), std::make_pair(0, 1))},
        {std::make_tuple(std::make_pair("1.0.", "0.9"), std::make_pair(0, 1))},
        {std::make_tuple(std::make_pair("1.0.0", "0.1_90"), std::make_pair(0, 1))},
        {std::make_tuple(std::make_pair("1", "0.1"), std::make_pair(0, 1))},
        {std::make_tuple(std::make_pair("1", "2"), std::make_pair(0, -1))},
        {std::make_tuple(std::make_pair("1.0.0-beta1", "1.0.0-beta2"), std::make_pair(0, -1))},

        // With parts parameter
        {std::make_tuple(std::make_pair("2.0.1.0_1", "2.0.1.0_2"), std::make_pair(5, -1))},
        {std::make_tuple(std::make_pair("2.0.1.0_1", "2.0.1.0_2"), std::make_pair(4, 0))},

        {std::make_tuple(std::make_pair("1.2_3.4", "1.3.4.5"), std::make_pair(3, -1))},
        {std::make_tuple(std::make_pair("1-2.3.4", "1.3.4.5"), std::make_pair(2, -1))},
        {std::make_tuple(std::make_pair("1.2.3.4", "1.3.4.5"), std::make_pair(1, 0))},

        {std::make_tuple(std::make_pair("1.5.0-alpha1", "1.5.0-alpha2"), std::make_pair(3, 0))}
    }));

    REQUIRE(utils::compareVersion(std::get<0>(data).first, std::get<0>(data).second,
        std::get<1>(data).first) == std::get<1>(data).second);

    // Swapped lhs and rhs
    REQUIRE(utils::compareVersion(std::get<0>(data).second, std::get<0>(data).first,
        std::get<1>(data).first) == (std::get<1>(data).second * -1));
}

TEST_CASE_METHOD(InitCleanupFixture, "Is valid version number", "[utils]")
{
    auto data = GENERATE(table<std::pair<std::string, bool>>({
        {std::make_tuple(std::make_pair("1.0.0", true))},
        {std::make_tuple(std::make_pair("1_0-0.975-3_45", true))},
        {std::make_tuple(std::make_pair("1.0.a", false))},
        {std::make_tuple(std::make_pair("1.0,2", false))},
        {std::make_tuple(std::make_pair("1.0,2", false))},
        {std::make_tuple(std::make_pair("1.0:2", false))},
        {std::make_tuple(std::make_pair("1.0;2", false))},
        {std::make_tuple(std::make_pair("1 0.2", false))},
        {std::make_tuple(std::make_pair("1|0.2", false))},
        {std::make_tuple(std::make_pair("...", false))},
        {std::make_tuple(std::make_pair("", false))},
    }));

    REQUIRE(utils::isVersionNum(std::get<0>(data).first) == std::get<0>(data).second);
}

TEST_CASE_METHOD(InitCleanupFixture, "Fetch username", "[utils]") {
    char user[] = "QTLICD_CLIENT_USERNAME=qt";
#if __linux__ || __APPLE__ || __MACH__
    putenv(user);
#else
    _putenv(user);
#endif
    std::string username;
    REQUIRE(utils::getUsernameFromEnv(username) == true);
    REQUIRE(utils::getUsernameFromOS(username) == true);
}

TEST_CASE_METHOD(InitCleanupFixture, "Get program path", "[utils]")
{
    REQUIRE(utils::fileExists(utils::getProgramPath()));
}

TEST_CASE_METHOD(InitCleanupFixture, "UTC time to string", "[utils]")
{
    const time_t validFromEpoch = 0x6597f3e7;
    const std::string validFromStr = utils::utcTimeToString(validFromEpoch, "%Y-%m-%dT%H:%M:%S");
    REQUIRE(validFromStr == "2024-01-05T12:19:51");

    const auto validFromEpochMs = createTimePoint(2024, 1, 5, 12, 19, 51, 123);
    const std::string validFromStrMs = utils::utcTimeMsToString(validFromEpochMs);
    REQUIRE(validFromStrMs == "2024-01-05T12:19:51.123Z");

    // verify no rounding occurs for the seconds
    const auto validFromEpochMsRounding = createTimePoint(2024, 1, 5, 12, 19, 51, 999);
    const std::string validFromStrMsRounding = utils::utcTimeMsToString(validFromEpochMsRounding);
    REQUIRE(validFromStrMsRounding == "2024-01-05T12:19:51.999Z");

    const auto validFromEpochNoMs = createTimePoint(2024, 1, 5, 12, 19, 51);
    const std::string validFromStrNoMs = utils::utcTimeMsToString(validFromEpochNoMs);
    REQUIRE(validFromStrNoMs == "2024-01-05T12:19:51.000Z");
}

TEST_CASE_METHOD(InitCleanupFixture, "String to UTC time", "[utils]")
{
    const std::string validFrom = "2024-01-05T12:19:51.708Z";
    const time_t validFromEpoch = utils::stringToUtcTime(validFrom.c_str(), "%Y-%m-%dT%H:%M:%S");
    REQUIRE(validFromEpoch == 0x6597f3e7);

    const auto validFromEpochMs = utils::stringToUtcTimeMs(validFrom);
    REQUIRE(validFromEpochMs == createTimePoint(2024, 1, 5, 12, 19, 51, 708));

    const auto validFromEpochNoMs = utils::stringToUtcTimeMs(validFrom.substr(0, validFrom.size() - 4));
    REQUIRE(validFromEpochNoMs == createTimePoint(2024, 1, 5, 12, 19, 51));
}

TEST_CASE_METHOD(InitCleanupFixture, "String compare", "[utils]")
{
    REQUIRE(utils::constTimeCompare("", "") == true);
    REQUIRE(utils::constTimeCompare("123", "") == false);
    REQUIRE(utils::constTimeCompare("123", "123") == true);
    REQUIRE(utils::constTimeCompare("123", "12345") == false);
    REQUIRE(utils::constTimeCompare("12356", "12345") == false);
}

TEST_CASE_METHOD(InitCleanupFixture, "Read file contents", "[utils]")
{
    static const std::string sc_expectedText =
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit,\n"
        "sed do eiusmod tempor incididunt ut labore et dolore magna\n"
        "aliqua. Ut enim ad minim veniam, quis nostrud exercitation\n"
        "ullamco laboris nisi ut aliquip ex ea commodo consequat.\n"
        "Duis aute irure dolor in reprehenderit in voluptate velit\n"
        "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint\n"
        "occaecat cupidatat non proident, sunt in culpa qui officia\n"
        "deserunt mollit anim id est laborum.\n";

    std::string contents;
    REQUIRE(utils::readFile(contents, TEST_FILE));
    REQUIRE(contents == sc_expectedText);

#if _WIN32
    static const std::string sc_expectedTextWinLineEndings =
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\n"
        "sed do eiusmod tempor incididunt ut labore et dolore magna\r\n"
        "aliqua. Ut enim ad minim veniam, quis nostrud exercitation\r\n"
        "ullamco laboris nisi ut aliquip ex ea commodo consequat.\r\n"
        "Duis aute irure dolor in reprehenderit in voluptate velit\r\n"
        "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint\r\n"
        "occaecat cupidatat non proident, sunt in culpa qui officia\r\n"
        "deserunt mollit anim id est laborum.";

    REQUIRE(utils::readFileWin32(contents, TEST_FILE));
    REQUIRE(contents == sc_expectedTextWinLineEndings);
#endif
}

TEST_CASE_METHOD(InitCleanupFixture, "Read OS architecture", "[utils]")
{
    std::string contents;
    REQUIRE(!utils::getOsArchitecture().empty());
}
