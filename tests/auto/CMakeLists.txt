# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

include_directories(${CMAKE_CURRENT_LIST_DIR}/../../src/libs/qlicensecore/
    ${CMAKE_CURRENT_LIST_DIR}/../../src/libs/qlicenseservice/
    ${CMAKE_CURRENT_LIST_DIR}/../../src/libs/qlicenseclient/
    ${CMAKE_CURRENT_LIST_DIR}/common/)

add_subdirectory(licenseservice)
add_subdirectory(licenseclient)
add_subdirectory(licensecache)
add_subdirectory(qtlicensetool)
add_subdirectory(utils)
add_subdirectory(lockfile)
add_subdirectory(qinifileparser)
add_subdirectory(qtaccount)
add_subdirectory(licdsetup)
add_subdirectory(installationmanager)
add_subdirectory(httpclient)
add_subdirectory(reservation)
add_subdirectory(timedeltachecker)
add_subdirectory(mocwrapper)
add_subdirectory(tcpclientserver)
