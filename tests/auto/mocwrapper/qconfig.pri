QT_ARCH = x86_64
QT_BUILDABI = x86_64-little_endian-lp64
QT_LIBCPP_ABI_TAG = 
QT.global.enabled_features = static pkg-config reduce_relocations signaling_nan zstd thread future concurrent dbus openssl-linked opensslv30 static static private_tests reduce_exports reduce_relocations openssl
QT.global.disabled_features = shared cross_compile debug_and_release separate_debug_info appstore-compliant simulator_and_device rpath force_asserts framework c++20 c++2a c++2b c++2b wasm-simd128 wasm-exceptions opensslv11
QT.global.disabled_features += release build_all
QT_CONFIG += static private_tests reduce_exports reduce_relocations openssl debug
CONFIG += debug  static plugin_manifest
QT_VERSION = 6.6.2
QT_MAJOR_VERSION = 6
QT_MINOR_VERSION = 6
QT_PATCH_VERSION = 2

QT_GCC_MAJOR_VERSION = 11
QT_GCC_MINOR_VERSION = 4
QT_GCC_PATCH_VERSION = 0
QT_EDITION = Open Source
