# Copyright (C) 2024 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

add_executable(tst_timedeltachecker tst_timedeltachecker.cpp)
target_link_libraries(tst_timedeltachecker PRIVATE Catch2 qlicenseservice)

add_test(NAME tst_timedeltachecker COMMAND tst_timedeltachecker)
