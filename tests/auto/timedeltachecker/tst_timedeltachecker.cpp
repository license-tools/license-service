/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include <timedeltachecker.h>
#include <utils.h>

using namespace QLicenseService;
using namespace QLicenseCore;

TEST_CASE_METHOD(InitCleanupFixture, "Allowed time delta", "[timedeltachecker]")
{
    std::string error;

    TimeDeltaChecker checker;
    REQUIRE(checker.allowedTimeDelta(error));

    std::string serverTimeString = "2024-01-05T12:19:51.708Z";
    std::string localTimeString = "2024-01-05T11:19:51.708Z";

    {
        // Local time is 1h behind
        checker.setTimes(utils::stringToUtcTime(serverTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"),
                         utils::stringToUtcTime(localTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"));
        REQUIRE(checker.allowedTimeDelta(error));
    }

    {
        // Cleared times allowed
        checker.reset();
        REQUIRE(checker.allowedTimeDelta(error));
    }

    {
        // Local time is 1h ahead
        serverTimeString = "2024-01-05T12:19:51.708Z";
        localTimeString = "2024-01-05T13:19:51.708Z";

        checker.setTimes(utils::stringToUtcTime(serverTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"),
                         utils::stringToUtcTime(localTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"));
        REQUIRE(checker.allowedTimeDelta(error));
    }

    {
        // Local time is 1h and 1 minute behind
        serverTimeString = "2024-01-05T12:19:51.708Z";
        localTimeString = "2024-01-05T11:18:51.708Z";

        checker.setTimes(utils::stringToUtcTime(serverTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"),
                         utils::stringToUtcTime(localTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"));
        REQUIRE(!checker.allowedTimeDelta(error));
    }

    {
        // Local time is 1h and 1s ahead
        serverTimeString = "2024-01-05T12:19:51.708Z";
        localTimeString = "2024-01-05T13:19:52.708Z";

        checker.setTimes(utils::stringToUtcTime(serverTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"),
                         utils::stringToUtcTime(localTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"));
        REQUIRE(!checker.allowedTimeDelta(error));
    }

    {
        // Local time is 24h behind
        serverTimeString = "2024-01-05T12:19:51.708Z";
        localTimeString = "2024-01-04T12:19:51.708Z";

        checker.setTimes(utils::stringToUtcTime(serverTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"),
                         utils::stringToUtcTime(localTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"));

        REQUIRE(!checker.allowedTimeDelta(error));
    }

    {
        // Local time is 24h ahead
        serverTimeString = "2024-01-05T12:19:51.708Z";
        localTimeString = "2024-01-06T12:19:51.708Z";

        checker.setTimes(utils::stringToUtcTime(serverTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"),
                         utils::stringToUtcTime(localTimeString.c_str(), "%Y-%m-%dT%H:%M:%S"));

        REQUIRE(!checker.allowedTimeDelta(error));
    }

}
