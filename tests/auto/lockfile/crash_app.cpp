/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#include <lockfile.h>
#include <utils.h>

#include <iostream>

using namespace QLicenseCore;

int main(int argc, char *argv[])
{
    std::cout << "Started application simulating crash" << std::endl;

    const std::string filename = utils::getUserHomeDir() + "/.tst_qtlicd.lock";

    QLicenseCore::LockFile *lock = new QLicenseCore::LockFile(filename);
    if (lock->tryLock())
        std::cout << "Locked test file" << std::endl;
    else
        std::cerr << "Could not lock test file!" << std::endl;

    std::cout << "Crashing now..." << std::endl;
    // crash due to access violation
    int *ptr = nullptr;
    *ptr = 123;
}
